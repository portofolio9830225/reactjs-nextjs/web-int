import axios from 'axios';
import authHeader from '../../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const readAll = async () => {
	return axios.get(`${API_URL_DEFAULT}product/all`, {
		headers: await authHeader(),
	});
};

export default { readAll };
