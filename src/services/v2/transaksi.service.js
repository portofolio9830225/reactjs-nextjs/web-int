import axios from 'axios';
import authHeader from '../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}transaksi`, payload, { headers: await authHeader() });
};

const read = async (query) => {
	return axios.get(`${API_URL_DEFAULT}transaksi`, {
		headers: await authHeader(),
		params: query,
	});
};

const readAdjust = async (page, perPage, query) => {
	return axios.get(`${API_URL_DEFAULT}transaksi/adjust?page=${page}&perPage=${perPage}${query}`, {
		headers: await authHeader(),
	});
};

const createAdjust = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}transaksi/adjust`, {
		headers: await authHeader(),
		data: payload,
	});
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}transaksi`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}transaksi`, {
		headers: await authHeader(),
		data: payload,
	});
};

export default { create, read, update, destroy, readAdjust, createAdjust };
