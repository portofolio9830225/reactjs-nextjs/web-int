import axios from 'axios';
import authHeader from '../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}customer`, payload, { headers: await authHeader() });
};

const createBulk = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}customer/bulk`, payload, { headers: await authHeader() });
};

const read = async (query) => {
	return axios.get(`${API_URL_DEFAULT}customer`, {
		headers: await authHeader(),
		params: query,
	});
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}customer`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}customer`, {
		headers: await authHeader(),
		data: payload,
	});
};

const generateExcel = async (query) => {
	return axios.get(`${API_URL_DEFAULT}customer/generate-excel`, {
		headers: await authHeader(),
		params: query,
		responseType: 'blob',
	});
};

export default { create, createBulk, read, update, destroy, generateExcel };
