import axios from 'axios';
import authHeader from '../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}data-customer`, payload, { headers: await authHeader() });
};

const createBulk = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}data-customer/bulk`, payload, {
		headers: await authHeader(),
	});
};

const read = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}data-customer/?${query_string}`, {
		headers: await authHeader(),
	});
};

const readBulk = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}data-customer/bulk/?${query_string}`, {
		headers: await authHeader(),
	});
};

export default {
	create,
	createBulk,
	read,
	readBulk,
};
