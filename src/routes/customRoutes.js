import React, { lazy } from 'react';

// define manual import for preventing hook restriction
const DUMMY = {
	CRUD: lazy(() => import('../views/CRUD')),
};

const Feature = {
	Customer: lazy(() => import('../views/v2/DataCustomer')),
};

const Transaction = {
	Adjustment: lazy(() => import('../views/v2/azhar/transaction/Adjustment')),
};

const contents = [
	{
		path: null,
		element: <DUMMY.CRUD />,
		index: 'CRUD',
		exact: true,
	},
	{
		path: null,
		element: <Feature.Customer />,
		index: 'Customer',
		exact: true,
	},
	{
		path: null,
		element: <Transaction.Adjustment />,
		index: 'AdjustTransaction',
		exact: true,
	},
];
export default contents;
