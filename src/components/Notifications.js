import Swal from 'sweetalert2';
import showNotification from './extras/showNotification';

const TYPE = {
	DEFAULT: 'default',
	INFO: 'info',
	SUCCESS: 'success',
	WARNING: 'warning',
	ERROR: 'error',
};

const TYPE_SWALL = {
	DEFAULT: 'default',
	INFO: 'info',
	SUCCESS: 'success',
	WARNING: 'warning',
	ERROR: 'error',
};

const TYPE_NOTIF = {
	DEFAULT: 'default',
	INFO: 'info',
	SUCCESS: 'success',
	WARNING: 'warning',
	ERROR: 'danger',
};

const showNotif = ({ header, message, type }) => {
	let type_swall = '';
	let type_notif = '';

	if (type === TYPE.DEFAULT) {
		type_swall = TYPE_SWALL.DEFAULT;
		type_notif = TYPE_NOTIF.DEFAULT;
	} else if (type === TYPE.INFO) {
		type_swall = TYPE_SWALL.INFO;
		type_notif = TYPE_NOTIF.INFO;
	} else if (type === TYPE.SUCCESS) {
		type_swall = TYPE_SWALL.SUCCESS;
		type_notif = TYPE_NOTIF.SUCCESS;
	} else if (type === TYPE.WARNING) {
		type_swall = TYPE_SWALL.WARNING;
		type_notif = TYPE_NOTIF.WARNING;
	} else if (type === TYPE.ERROR) {
		type_swall = TYPE_SWALL.ERROR;
		type_notif = TYPE_NOTIF.ERROR;
	}

	Swal.fire(header, message, type_swall);
	showNotification(header, message, type_notif);
};

export default { showNotif, TYPE };
