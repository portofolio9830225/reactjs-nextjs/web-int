import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import COLORS from '../common/data/enumColors';

const CustomSelect = ({
	options,
	onChange,
	value,
	isMulti,
	isSearchable,
	darkTheme,
	isValid,
	isDisabled,
	placeholder,
}) => {
	const theme_select = (theme) => ({
		...theme,
		colors: {
			...theme.colors,
			primary25: darkTheme ? '#606AEB' : '#DEEBFF',
			primary: darkTheme ? '#3942BF' : '#3942BF',
		},
	});
	const border_color_select = (valid, disable) => {
		if (disable) return darkTheme ? '#424242' : '#DBDBDB';
		if (valid) return '#DEDEDE';
		return COLORS.DARK.code;
	};
	const menu_color_select = (disable) => {
		if (disable) return darkTheme ? '#343A40' : '#E9ECEF';
		return darkTheme ? '#242731' : 'white';
	};
	const styles_select = {
		control: (styles) => ({
			...styles,
			color: !darkTheme ? '#242731' : 'white',
			backgroundColor: menu_color_select(isDisabled),
			borderColor: border_color_select(isValid, isDisabled),
			'&:hover': {
				borderColor: darkTheme ? COLORS.INFO.code : COLORS.DARK.code,
			},
		}),
		menu: (styles) => ({
			...styles,
			color: 'white',
			backgroundColor: '#242731',
		}),
		menuList: (styles) => ({
			...styles,
			color: !darkTheme ? '#242731' : 'white',
			backgroundColor: darkTheme ? '#242731' : 'white',
		}),
		singleValue: (styles) => {
			return {
				...styles,
				color: !darkTheme ? '#242731' : 'white',
				backgroundColor: darkTheme ? '#242731' : 'white',
			};
		},
		multiValue: (styles) => {
			return {
				...styles,
				backgroundColor: '#CCFFFF',
			};
		},
		multiValueLabel: (styles) => ({
			...styles,
			color: '#242731',
		}),
		multiValueRemove: (styles) => ({
			...styles,
			color: darkTheme ? '#242731' : 'white',
			backgroundColor: '#F76A76',
			':hover': {
				color: !darkTheme ? '#242731' : 'white',
				backgroundColor: darkTheme ? '#ED4A57' : '#FF4A4A',
			},
		}),
	};

	return (
		<div>
			<Select
				options={options}
				onChange={onChange}
				isDisabled={isDisabled}
				placeholder={placeholder}
				isSearchable={isSearchable}
				value={value}
				isMulti={isMulti}
				styles={styles_select}
				theme={theme_select}
			/>
		</div>
	);
};

CustomSelect.propTypes = {
	options: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	onChange: PropTypes.func,
	value: PropTypes.instanceOf(Object),
	isMulti: PropTypes.bool,
	isSearchable: PropTypes.bool,
	darkTheme: PropTypes.bool,
	isValid: PropTypes.bool,
	isDisabled: PropTypes.bool,
	placeholder: PropTypes.string,
};
CustomSelect.defaultProps = {
	options: [],
	onChange: null,
	value: [],
	isMulti: false,
	isSearchable: false,
	darkTheme: false,
	isValid: true,
	isDisabled: false,
	placeholder: 'Select...',
};

export default CustomSelect;
