import React, { useState, useRef } from 'react';
import AsyncCreatableSelect from 'react-select/async-creatable';
import PropTypes, { arrayOf } from 'prop-types';

const DynamicSelect = ({
	data,
	select_items,
	onChange,
	readOnly,
	isEdit,
	errors,
	defaultOptions,
	loadOptions,
	placeholder,
	darkTheme,
}) => {
	const inputRef = useRef(null);
	const [setInput] = useState('');

	// to send inserted data back to parent when there are some changes
	const handleOnChange = (newValue) => {
		let new_data = [...data];
		const new_select_items = [...select_items];
		const val = newValue.map((v) => v.label);
		new_data = val;
		onChange(new_data, new_select_items, isEdit);
	};

	return (
		<>
			<div>
				{' '}
				<AsyncCreatableSelect
					cacheOptions
					defaultOptions
					ref={inputRef}
					loadOptions={(inputRaw) => loadOptions(inputRaw)}
					styles={{
						multiValueRemove: (base) => ({
							...base,
							display: readOnly ? 'none' : '',
						}),
						multiValue: (styles) => {
							return {
								...styles,
								backgroundColor: '#2AA9BE',
								color: 'white',
							};
						},
						control: (styles) => ({
							...styles,
							backgroundColor: darkTheme ? '#242731' : 'white',
						}),
						menu: (styles) => ({
							...styles,
							color: 'black',
						}),
						multiValueLabel: (styles) => ({
							...styles,
							color: 'white',
							fontWeight: 'bold',
						}),
					}}
					key={JSON.stringify(data)}
					isMulti
					onChange={handleOnChange}
					isDisabled={readOnly}
					placeholder={placeholder}
					isClearable={!readOnly}
					onInputChange={setInput}
					defaultValue={
						// eslint-disable-next-line no-nested-ternary
						defaultOptions !== true
							? select_items.filter((e) => {
									return data.some((dt) => dt == e.label);
							  })
							: data
							? data.map((dt, idx) => {
									return {
										value: idx,
										label: dt,
									};
							  })
							: null
					}
				/>
			</div>
			{errors.data && (
				<div
					style={{
						width: '100%',
						marginTop: '.25rem',
						fontSize: '80%',
						color: ' #dc3545',
					}}>
					{errors.data}{' '}
				</div>
			)}
		</>
	);
};

DynamicSelect.propTypes = {
	data: arrayOf(PropTypes.string),
	select_items: arrayOf(PropTypes.instanceOf(Object)),
	onChange: PropTypes.func,
	readOnly: PropTypes.bool,
	isEdit: PropTypes.bool,
	errors: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	defaultOptions: PropTypes.bool,
	loadOptions: PropTypes.func,
	placeholder: PropTypes.string,
	darkTheme: PropTypes.bool,
};
DynamicSelect.defaultProps = {
	data: [],
	select_items: [],
	onChange: null,
	readOnly: false,
	isEdit: false,
	errors: null,
	defaultOptions: true,
	loadOptions: null,
	placeholder: '',
	darkTheme: false,
};
export default DynamicSelect;
