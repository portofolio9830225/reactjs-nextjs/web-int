import React from 'react';
import { ModalContext } from './modalContext';
import Button from './bootstrap/Button';

const DynamicModal = (dt) => {
	const { handleModal } = React.useContext(ModalContext);
	const { content } = dt;
	return (
		<Button
			icon='Edit'
			isOutline
			type='button'
			color='primary'
			onClick={() => handleModal(content)}>
			Edit
		</Button>
	);
};

export default DynamicModal;
