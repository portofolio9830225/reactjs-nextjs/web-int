import React from 'react';
import {
	FaCaretRight,
	FaCaretDown,
	FaRegWindowClose,
	FaRegTrashAlt,
	FaRegEdit,
	FaRegCheckSquare,
	FaFolder,
	FaFolderOpen,
} from 'react-icons/fa';
import PropTypes from 'prop-types';
import FolderTree from 'react-folder-tree';

import 'react-folder-tree/dist/style.css';

const CaretRightIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaCaretRight onClick={handleClick} />;
};

const CaretDownIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaCaretDown onClick={handleClick} />;
};

const FileIcon = () => null;

const FolderIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaFolder onClick={handleClick} />;
};

const FolderOpenIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaFolderOpen onClick={handleClick} />;
};

const EditIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaRegEdit onClick={handleClick} />;
};

const DeleteIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaRegTrashAlt onClick={handleClick} />;
};

const CancelIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaRegWindowClose onClick={handleClick} />;
};

const OKIcon = ({ onClick: defaultOnClick }) => {
	const handleClick = () => {
		defaultOnClick();
	};

	return <FaRegCheckSquare onClick={handleClick} />;
};

const iconComponents = {
	FileIcon,
	FolderIcon,
	FolderOpenIcon,
	EditIcon,
	DeleteIcon,
	CancelIcon,
	CaretRightIcon,
	CaretDownIcon,
	OKIcon,
};

const CustomIconsTree = ({ treeState, onChange }) => {
	const customIdentPixels = 50;

	return (
		<FolderTree
			data={treeState}
			showCheckbox={false}
			iconComponents={iconComponents}
			indentPixels={customIdentPixels}
			onChange={onChange}
		/>
	);
};

CaretRightIcon.propTypes = {
	onClick: PropTypes.func,
};
CaretRightIcon.defaultProps = {
	onClick: null,
};
CaretDownIcon.propTypes = {
	onClick: PropTypes.func,
};
CaretDownIcon.defaultProps = {
	onClick: null,
};
FolderIcon.propTypes = {
	onClick: PropTypes.func,
};
FolderIcon.defaultProps = {
	onClick: null,
};
FolderOpenIcon.propTypes = {
	onClick: PropTypes.func,
};
FolderOpenIcon.defaultProps = {
	onClick: null,
};
EditIcon.propTypes = {
	onClick: PropTypes.func,
};
EditIcon.defaultProps = {
	onClick: null,
};
DeleteIcon.propTypes = {
	onClick: PropTypes.func,
};
DeleteIcon.defaultProps = {
	onClick: null,
};
CancelIcon.propTypes = {
	onClick: PropTypes.func,
};
CancelIcon.defaultProps = {
	onClick: null,
};
OKIcon.propTypes = {
	onClick: PropTypes.func,
};
OKIcon.defaultProps = {
	onClick: null,
};
CustomIconsTree.propTypes = {
	treeState: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
};
CustomIconsTree.defaultProps = {
	treeState: null,
};
CustomIconsTree.propTypes = {
	onChange: PropTypes.func,
};
CustomIconsTree.defaultProps = {
	onChange: null,
};

export default CustomIconsTree;
