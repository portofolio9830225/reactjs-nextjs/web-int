import React from 'react';
import PropTypes from 'prop-types';
import * as XLSX from 'xlsx';
import ExcelJS from 'exceljs';
import moment from 'moment';
import Card, { CardTabItem } from '../bootstrap/Card';
import { getColumnName } from '../../helpers/helpers';

const isEmptyObject = (row) => {
	const countObject = row.join('').length;
	return countObject === 0;
};

const dataExcel = {
	sheets: {
		customer: {
			name: 'Customer',
			columns_name: [
				'customer_name',
				'country',
				'contact_person',
				'position',
				'email_phone',
				'top',
				'product_grade',
				'packing_size',
				'fob_price',
				'qty_year_1',
				'qty_year_2',
				'full_potential',
				'potential_per_month',
				'potential_per_year',
				'share_year_1',
				'share_year_2',
				'remarks',
				'comercial_lost',
				'sales_person',
			],
		},
		respond: {
			name: 'Respond',
			columns_name: ['company', 'country', 'latest_status', 'pic', 'email', 'phone'],
		},
	},
	file: {
		name: 'template_customer_information.xlsx',
	},
};

// const defaultTemplate = [];

export const readFileExcelFormat = (file) => {
	return new Promise((resolve, reject) => {
		try {
			const fileReader = new FileReader();
			fileReader.readAsArrayBuffer(file);

			fileReader.onload = (e) => {
				const binaryString = e.target.result;
				const workbook = XLSX.read(binaryString, { type: 'buffer' });

				const worksheet_customer = workbook.Sheets[dataExcel?.sheets?.customer?.name];

				XLSX.utils.sheet_add_aoa(
					worksheet_customer,
					[['qty_year_1', 'qty_year_2', 'c', 'd', 'e', 'share_year_1', 'share_year_2']],
					{ origin: 'J2' },
				);
				const data_customer_options = XLSX.utils.sheet_to_json(worksheet_customer, {
					range: 'J2:P3',
				});

				const data_customer = XLSX.utils.sheet_to_json(worksheet_customer, {
					header: 1,
					defval: '',
				});

				// START calculate data sheet customer
				const customer_index = [];
				const data_customer_object = [];
				const { customer } = dataExcel.sheets;

				// let check_end_data = false;
				for (let indexRow = 0; indexRow < data_customer.length; indexRow += 1) {
					if (customer_index.length === 0) {
						customer_index.push({
							index: indexRow + 4,
							desc: 'value',
						});
					}
					if (indexRow + 1 < data_customer.length) {
						if (
							isEmptyObject(data_customer.at(indexRow)) &&
							!isEmptyObject(data_customer.at(indexRow + 1))
						) {
							customer_index.push({
								index: indexRow + 2,
								desc: 'value',
							});
						}
					} else {
						customer_index.push({
							index: indexRow + 2,
							desc: 'value',
						});
					}
				}

				// loop appen data
				customer_index.forEach((item, index) => {
					if (customer_index.length - 1 === index) {
						return;
					}
					// check data value not header or footer
					if (item.desc === 'value') {
						XLSX.utils.sheet_add_aoa(worksheet_customer, [customer.columns_name], {
							origin: `A${item.index}`,
						});
						// find data
						const range_row_sheet = `A${item.index}:S${
							customer_index.at(index + 1).index - 1
						}`;
						// [A] first column key, [S] last column key
						const data_row_sheet = XLSX.utils.sheet_to_json(worksheet_customer, {
							range: range_row_sheet,
						});
						// add data if not null
						if (data_row_sheet.length !== 0) {
							data_customer_object.push(data_row_sheet);
						}
					}
				});
				// END calculate data sheet customer

				resolve({
					customer: data_customer_object,
					customer_opt: data_customer_options.at(0),
				});

				// resolve({});
			};
		} catch (e) {
			reject(new Error(e.message));
		}
	});
};

export const readFileExcel = (file) => {
	return new Promise((resolve, reject) => {
		try {
			const fileReader = new FileReader();
			fileReader.readAsArrayBuffer(file);

			fileReader.onload = (e) => {
				const binaryString = e.target.result;
				const workbook = XLSX.read(binaryString, { type: 'buffer' });

				const worksheets = workbook.SheetNames.map((item) => {
					const worksheet = workbook.Sheets[item];
					return {
						sheetName: item,
						workSheet: XLSX.utils.sheet_to_json(worksheet, {
							header: 1,
							defval: '',
						}),
					};
				});
				resolve(worksheets);
			};
		} catch (e) {
			reject(new Error(e.message));
		}
	});
};

export const writeFileExcel = async () => {
	try {
		// proc
		const workbook = new ExcelJS.Workbook();
		const worksheet = workbook.addWorksheet(dataExcel.sheets.customer.name);

		const data = [
			[],
			// header 1
			[
				'Last Update : ',
				moment().format('MMM DD YYYY'),
				'Note: add a blank line before adding the second new data and so on.',
			],
			// header 2
			[
				'CUSTOMER',
				'COUNTRY',
				'CONTACT',
				'POSITION',
				'EMAIL',
				'T.O.P',
				'PRODUCT',
				'PACKING',
				'FOB',
				moment().add(-1, 'years').format('YYYY'),
				moment().format('YYYY'),
				'Full Potential',
				'POTENTIAL SHARE',
				'',
				moment().format('YYYY'),
				moment().format('YYYY').concat(' ', 'SHARE'),
				'REMARKS',
				'Commercial Lost',
				'Sales Person',
			],
			// header 3
			[
				'NAME',
				'',
				'PERSON',
				'',
				'PHONE',
				'',
				'GRADE',
				'SIZE (KG)',
				'PRICE/KG',
				'QTY (MT)',
				'',
				'',
				'Per Month',
				'Per year',
			],
		];

		for (let i = 0; i < 5; i += 1) {
			data.push([
				'customer name',
				'country',
				'contact name 1',
				'position 1',
				'email 1',
				'value',
				'text',
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				'remarks',
				0,
				'LNK000XXX',
			]);
			data.push([
				'',
				'',
				'contact name 2',
				'position 2',
				'email 2',
				'value',
				'text',
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				'',
				0,
				'',
			]);
			data.push([
				'',
				'',
				'contact name 3',
				'position 3',
				'email 3',
				'value',
				'text',
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				'',
				0,
				'',
			]);
			data.push(['', '', '', '', 'address/phone/etc']);
			data.push(['', '', '', '', 'address/phone/etc']);
			data.push(['', '', '', '', 'address/phone/etc']);
			data.push([]);
		}

		worksheet.addRows(data);

		for (let indexRow = 3; indexRow < 45; indexRow += 1) {
			data.at(2).forEach((item, indexCol) => {
				const col_char = String.fromCharCode('A'.charCodeAt(0) + indexCol).concat(indexRow);
				const cell = worksheet.getCell(col_char);
				cell.alignment = { vertical: 'middle', horizontal: 'center' };
				cell.font = { size: 11 };
				// color header
				if (indexRow === 3 || indexRow === 4) {
					cell.font = { bold: true };
					cell.fill = {
						type: 'pattern',
						pattern: 'solid',
						bgColor: { argb: 'FFFFFF00' },
						fgColor: { argb: 'FFFFFF00' },
					};
				}
				// color commercial lost & sales person
				if (indexCol >= data.at(2).length - 2) {
					cell.fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'FFE4DFEC' },
					};
				}
				// color different new data

				if ((indexRow - 5) % 7 === 0) {
					cell.fill = {
						type: 'pattern',
						pattern: 'solid',
						fgColor: { argb: 'FF92CDDC' },
					};
				}
			});
		}

		worksheet.getCell('A5').note = 'Text value and required';
		worksheet.getCell('B5').note = 'Text value and required';
		worksheet.getCell('C5').note = 'Text value and required';
		worksheet.getCell('D5').note = 'Text value';
		worksheet.getCell('E5').note = 'Text value';
		worksheet.getCell('F5').note = 'Text value';
		worksheet.getCell('G5').note = 'Text value';
		worksheet.getCell('H5').note = 'Number value';
		worksheet.getCell('I5').note = 'Number value';
		worksheet.getCell('J5').note = 'Number value';
		worksheet.getCell('K5').note = 'Number value';
		worksheet.getCell('L5').note = 'Number value';
		worksheet.getCell('M5').note = 'Number value';
		worksheet.getCell('N5').note = 'Number value';
		worksheet.getCell('O5').note = 'Number value';
		worksheet.getCell('P5').note = 'Number value';
		worksheet.getCell('Q5').note = 'Text value';
		worksheet.getCell('R5').note = 'Number value';
		worksheet.getCell('S5').note = 'NIK Value and required';

		worksheet.mergeCells('M3:N3');
		worksheet.mergeCells('C2:D2');

		worksheet.getColumn(1).width = 60;
		worksheet.getColumn(2).width = 18;
		worksheet.getColumn(3).width = 35;
		worksheet.getColumn(4).width = 32;
		worksheet.getColumn(5).width = 94;
		worksheet.getColumn(6).width = 68;
		worksheet.getColumn(7).width = 28;
		worksheet.getColumn(8).width = 9;
		worksheet.getColumn(9).width = 12;
		worksheet.getColumn(10).width = 9;
		worksheet.getColumn(11).width = 8;
		worksheet.getColumn(12).width = 13;
		worksheet.getColumn(13).width = 17;
		worksheet.getColumn(14).width = 9;
		worksheet.getColumn(15).width = 8;
		worksheet.getColumn(16).width = 14;
		worksheet.getColumn(17).width = 66;
		worksheet.getColumn(18).width = 17;
		worksheet.getColumn(19).width = 14;

		worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'right' };
		worksheet.getCell('B2').alignment = { vertical: 'middle', horizontal: 'right' };
		worksheet.getCell('A2').font = { size: 11, color: { argb: 'FFFF1100' } };
		worksheet.getCell('B2').font = { size: 11, color: { argb: 'FFFF1100' } };

		worksheet.getCell('A23').font = { size: 11, color: { argb: 'FFFF1100' } };

		worksheet.getRow(3).commit();

		const file = await workbook.xlsx.writeBuffer(dataExcel.file.name);

		return Promise.resolve({ file, file_name: dataExcel.file.name });
	} catch (e) {
		return Promise.reject(new Error(e.message));
	}
};

const WorksheetContent = ({ data }) => {
	return (
		<div className='table-responsive' style={{ maxHeight: '500px' }}>
			<table className='table table-sm table-striped table-bordered'>
				<thead>
					<tr>
						<th style={{ textAlign: 'center', whiteSpace: 'nowrap' }}>#</th>
						{data.length > 0 &&
							data[0]?.map((item, index) => (
								<th
									key={'th'.concat(index)}
									style={{ textAlign: 'center', whiteSpace: 'nowrap' }}>
									{getColumnName(index)}
								</th>
							))}
					</tr>
				</thead>
				<tbody>
					{data?.map((itemRow, indexRow) => (
						<tr key={'item-row'.concat(indexRow)}>
							<th key={'item-row'.concat(indexRow, 'item-col')} scope='col'>
								{indexRow + 1}
							</th>
							{itemRow?.map((itemCol, indexCol) => (
								<td
									key={'item-row'.concat(indexRow, 'item-col', indexCol)}
									style={{ whiteSpace: 'nowrap' }}>
									{itemCol}
								</td>
							))}
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

WorksheetContent.propTypes = {
	data: PropTypes.instanceOf(Array),
};
WorksheetContent.defaultProps = {
	data: [],
};

const Worksheet = ({ data, ...props }) => {
	return (
		// eslint-disable-next-line react/jsx-props-no-spreading
		<Card shadow='sm' hasTab {...props}>
			{data?.map((item, index) => (
				<CardTabItem
					key={'card-item-'.concat(index)}
					id={item.sheetName}
					title={item.sheetName}>
					<WorksheetContent data={item.workSheet} />
				</CardTabItem>
			))}
		</Card>
	);
};

Worksheet.propTypes = {
	data: PropTypes.instanceOf(Array),
};
Worksheet.defaultProps = {
	data: [],
};

export default Worksheet;
