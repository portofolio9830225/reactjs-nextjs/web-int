import React from 'react';
import PropTypes from 'prop-types';
import Modal, { ModalBody, ModalFooter, ModalHeader, ModalTitle } from './bootstrap/Modal';

const standardModal = ({ modalSize, modalTitle, isOpen, setIsOpen }) => {
	return (
		<Modal isOpen={isOpen} setIsOpen={setIsOpen} size={modalSize}>
			<ModalHeader setIsOpen={setIsOpen} className='p-4'>
				<ModalTitle>{modalTitle}</ModalTitle>
			</ModalHeader>
			<ModalBody className='px-4'>{modalContent}</ModalBody>
			<ModalFooter className='px-4 pb-4'>{modalFooter}</ModalFooter>
		</Modal>
	);
};

standardModal.propTypes = {
	modalSize: PropTypes.string.isRequired,
	modalTitle: PropTypes.string.isRequired,
	modalContent: PropTypes.node.isRequired,
	modalFooter: PropTypes.node.isRequired,
	isOpen: PropTypes.bool.isRequired,
	setIsOpen: PropTypes.func.isRequired,
};

export default standardModal;
