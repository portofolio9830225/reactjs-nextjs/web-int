import React, { useState, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import moment from 'moment';
import ExcelJS from 'exceljs';
import * as XLSX from 'xlsx';
import FileDownload from 'js-file-download';
import Page from '../../layout/Page/Page';
import PageWrapper from '../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardActions,
	CardBody,
	CardFooter,
	CardFooterLeft,
	CardFooterRight,
	CardHeader,
	CardLabel,
	CardTabItem,
	CardTitle,
} from '../../components/bootstrap/Card';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../../components/bootstrap/Modal';
import Accordion, { AccordionItem } from '../../components/bootstrap/Accordion';
import Button, { ButtonGroup } from '../../components/bootstrap/Button';
import useDarkMode from '../../hooks/useDarkMode';
import Spinner from '../../components/bootstrap/Spinner';
import FormGroup from '../../components/bootstrap/forms/FormGroup';
import Input from '../../components/bootstrap/forms/Input';
import showNotification from '../../components/extras/showNotification';
import { getRequester } from '../../helpers/helpers';
import DataCustomerModule from '../../modules/DataCustomerModule';
import DarkDataTable from '../../components/DarkDataTable';
import ListGroup, { ListGroupItem } from '../../components/bootstrap/ListGroup';

const handleSubmit = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				DataCustomerModule.create(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been saved successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleSubmitBulk = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				DataCustomerModule.createBulk(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been saved successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const isEmptyObject = (row) => {
	const countObject = row.join('').length;
	return countObject === 0;
};

const TablePreview = ({ data }) => {
	const { darkModeStatus } = useDarkMode();
	const { sheets, data_excel } = data;

	const [activeTab, setActiveTab] = useState(sheets.at(0));

	return (
		<Card stretch shadow='sm'>
			<CardHeader>
				<CardActions>
					<ButtonGroup size='sm'>
						{sheets &&
							sheets.map((item) => (
								<Button
									key={item.name}
									type='button'
									color='info'
									isOutline={darkModeStatus}
									isLink={activeTab !== item}
									isLight={activeTab === item}
									onClick={() => setActiveTab(item)}>
									{item.name}
								</Button>
							))}
					</ButtonGroup>
				</CardActions>
			</CardHeader>
			<CardBody>
				<div
					className='table-responsive overflow-auto'
					style={{
						maxHeight: 400,
					}}>
					<table className='table table-modern'>
						<thead>
							<tr>
								{data_excel[activeTab.name] &&
									Object.keys(data_excel[activeTab.name].at(0)).map((item) => (
										<th key={item} style={{ whiteSpace: 'nowrap' }}>
											{item}
										</th>
									))}
							</tr>
						</thead>
						<tbody>
							{data_excel[activeTab.name] &&
								data_excel[activeTab.name].map((row, indexRow) => (
									<tr key={'col'.concat(indexRow)}>
										{data_excel[activeTab.name] &&
											Object.keys(data_excel[activeTab.name].at(0)).map(
												(key, indexCol) => (
													<td
														key={'row'.concat(
															indexRow,
															'col',
															indexCol,
														)}
														className='flex-nowrap'
														style={{ whiteSpace: 'nowrap' }}>
														{row[key]}
													</td>
												),
											)}
									</tr>
								))}
						</tbody>
					</table>
				</div>
			</CardBody>
		</Card>
	);
};

TablePreview.propTypes = {
	data: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
};
TablePreview.defaultProps = {
	data: null,
};

const FormDataCustomer = ({ initialValues, loading, setLoading, handleCustomSubmitBulk }) => {
	const { t } = useTranslation('crud');
	const { darkModeStatus } = useDarkMode();
	const { requester, form_file, data_excel } = initialValues;
	const { department } = getRequester();

	const [loadingDownload, setLoadingDownload] = useState(false);

	const [preview, setPreview] = useState(false);
	const [dataPreview, setDataPreview] = useState();

	const validate = (values) => {
		const errors = {};

		if (!values.form_file) {
			errors.form_file = 'Required';
		} else if (values.form_file.split('.').pop() !== 'xlsx') {
			errors.form_file = 'File must be .xlsx';
		}

		return errors;
	};

	const formikBulk = useFormik({
		initialValues: { form_file },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.isConfirmed) {
					onCustomSubmitBulk(values, { setSubmitting, resetForm, setErrors });
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			});
		},
	});

	const onCustomChange = (e) => {
		formikBulk.handleChange(e);

		const file_name = document.getElementById('form_file');
		const file = file_name.files[0];

		if (file.name.split('.').pop() === 'xlsx') {
			setPreview(true);
			readExcelPreview(file)
				.then((response) => {
					setDataPreview(response);
				})
				.catch(() => {
					showNotification('Information', 'Excel format not same', 'danger');
					setLoading(false);
				});
		} else {
			setPreview(false);
		}
	};

	const onCustomSubmitBulk = (_values, { setSubmitting, resetForm, setErrors }) => {
		try {
			setLoading(true);

			const file_name = document.getElementById('form_file');
			const file = file_name.files[0];

			readExcel(file)
				.then((response) => {
					const { customer, customer_opt } = response;

					// INIT
					const list_customer = getDataCustomerBulk(customer, customer_opt);

					if (list_customer.length === 0) {
						showNotification('Information', 'Data Customer is null', 'danger');
						setLoading(false);
						return;
					}

					const result_check = check_data_parameter(list_customer);

					if (result_check.error) {
						showNotification('Information', result_check.message, 'danger');
						setLoading(false);
						return;
					}

					const payload = { requester, department, list_customer };

					handleCustomSubmitBulk(payload);
					resetForm({ form_file });
					setPreview(false);
				})
				.catch(() => {
					showNotification('Information', 'Excel format not same', 'danger');
					setLoading(false);
				});
		} catch (error) {
			setErrors({ submit: error.message });
		} finally {
			setSubmitting(false);
		}
	};

	const check_data_parameter = (_list_customer) => {
		let status = { error: false, message: '' };

		for (let index = 0; index < _list_customer.length; index += 1) {
			if (!_list_customer.at(index).customer_name) {
				status = { error: true, message: 'customer name is required' };
				break;
			}
			if (!_list_customer.at(index).country) {
				status = { error: true, message: 'country is required' };
				break;
			}
			if (!_list_customer.at(index).sales_person) {
				status = { error: true, message: 'sales person is required' };
				break;
			}
		}

		return status;
	};

	const getDataCustomerBulk = (_data_customer_bulk, _data_customer_bulk_opt) => {
		const data_customer = [];

		if (_data_customer_bulk.length === 0) {
			return [];
		}

		_data_customer_bulk.forEach((customer) => {
			const new_customer = getDataCustomer(customer);

			const _new_customer = {
				...new_customer,
				qty_year_1: Number(_data_customer_bulk_opt.qty_year_1),
				qty_year_2: Number(_data_customer_bulk_opt.qty_year_2),
			};

			data_customer.push(_new_customer);
		});

		return data_customer;
	};

	const getDataCustomer = (_data_customer) => {
		const new_data_customer = {};
		const contact_person = [];
		const contact_detail = [];
		const details = [];

		_data_customer.forEach((item) => {
			// init start
			// field customer name
			if (!new_data_customer.customer_name && item.customer_name) {
				new_data_customer.customer_name = item.customer_name;
			}
			// field country
			if (!new_data_customer.country && item.country) {
				new_data_customer.country = item.country;
			}
			// field contact person & position & email
			if (item.contact_person && !item.contact_person.includes('--')) {
				const new_contact_person = {};
				new_contact_person.name = item.contact_person;
				if (item.position) {
					new_contact_person.position = item.position;
				}
				if (item.email_phone && item.email_phone.includes('@')) {
					new_contact_person.email = item.email_phone;
				} else {
					contact_detail.push(item.email_phone);
				}
				contact_person.push(new_contact_person);
			}
			// field contact
			if (!item.contact_person && !item.position && item.email_phone) {
				contact_detail.push(item.email_phone);
			}
			// field top
			if (!new_data_customer.top && item.top) {
				new_data_customer.top = item.top;
			}
			// field top
			if (!new_data_customer.comercial_lost && item.comercial_lost) {
				new_data_customer.comercial_lost = item.comercial_lost;
			}
			// field sales person
			if (!new_data_customer.sales_person && item.sales_person) {
				new_data_customer.sales_person = item.sales_person;
			}

			// init details
			const new_details = {};
			// product grade
			if (item.product_grade) {
				new_details.product_grade = item.product_grade;
			}
			// packing_size (KG)
			if (item.packing_size) {
				new_details.packing_size = item.packing_size;
			}
			// fob_price ($)
			if (item.fob_price) {
				new_details.fob_price = item.fob_price;
			}
			// qty_year_1 (MT)
			if (item.qty_year_1) {
				new_details.qty_year_1 = item.qty_year_1;
			}
			// qty_year_2 (MT)
			if (item.qty_year_2) {
				new_details.qty_year_2 = item.qty_year_2;
			}
			// full_potential
			if (item.full_potential) {
				new_details.full_potential = item.full_potential;
			}
			// potential_per_month
			if (item.potential_per_month) {
				new_details.potential_per_month = item.potential_per_month;
			}
			// potential_per_year
			if (item.potential_per_year) {
				new_details.potential_per_year = item.potential_per_year;
			}
			// share_year_1
			if (item.share_year_1) {
				new_details.share_year_1 = item.share_year_1;
			}
			// share_year_2
			if (item.share_year_2) {
				new_details.share_year_2 = item.share_year_2;
			}
			// remarks
			if (item.remarks) {
				new_details.remarks = item.remarks;
			}

			if (Object.keys(new_details).length !== 0) {
				details.push(new_details);
			}
		});

		new_data_customer.contact_person = contact_person;
		new_data_customer.contact_detail = contact_detail;
		new_data_customer.details = details;

		return new_data_customer;
	};

	const readExcel = (file) => {
		const newResponse = new Promise((resolve, reject) => {
			try {
				const fileReader = new FileReader();
				fileReader.readAsArrayBuffer(file);

				fileReader.onload = (e) => {
					const bufferArray = e.target.result;

					const workbook = XLSX.readFile(bufferArray, { type: 'buffer' });

					const worksheet_customer = workbook.Sheets[data_excel.sheets.customer.name];
					const worksheet_respond = workbook.Sheets[data_excel.sheets.respond.name];

					XLSX.utils.sheet_add_aoa(
						worksheet_customer,
						[
							[
								'qty_year_1',
								'qty_year_2',
								'c',
								'd',
								'e',
								'share_year_1',
								'share_year_2',
							],
						],
						{ origin: 'J2' },
					);
					const data_customer_options = XLSX.utils.sheet_to_json(worksheet_customer, {
						range: 'J2:P3',
					});

					const data_customer = XLSX.utils.sheet_to_json(worksheet_customer, {
						header: 1,
						defval: '',
					});

					const data_respond = XLSX.utils.sheet_to_json(worksheet_respond, {
						header: 1,
						defval: '',
					});

					// START calculate data sheet customer
					const customer_index = [];
					const data_customer_object = [];
					const { customer } = data_excel.sheets;

					let check_end_data = false;
					for (let indexRow = 0; indexRow < data_customer.length; indexRow += 1) {
						const check_end_string = data_customer.at(indexRow).at(0).toLowerCase();
						if (
							check_end_string === 'end' ||
							check_end_string === 'end data' ||
							check_end_string === 'regular running'
						) {
							check_end_data = true;
							break;
						}
						if (customer_index.length === 0) {
							customer_index.push({
								index: indexRow + 4,
								desc: 'value',
							});
						}
						if (indexRow + 1 < data_customer.length) {
							if (
								isEmptyObject(data_customer.at(indexRow)) &&
								!isEmptyObject(data_customer.at(indexRow + 1))
							) {
								customer_index.push({
									index: indexRow + 2,
									desc: 'value',
								});
							}
						} else {
							customer_index.push({
								index: indexRow + 2,
								desc: 'value',
							});
						}
					}

					// 'end data' or 'Regular running' not found is error
					if (!check_end_data) {
						reject(new Error({ error: true }));
					}

					// loop appen data
					customer_index.forEach((item, index) => {
						if (customer_index.length - 1 === index) {
							return;
						}
						// check data value not header or footer
						if (item.desc === 'value') {
							XLSX.utils.sheet_add_aoa(worksheet_customer, [customer.columns_name], {
								origin: `A${item.index}`,
							});
							// find data
							const range_row_sheet = `A${item.index}:S${
								customer_index.at(index + 1).index - 1
							}`;
							// [A] first column key, [S] last column key
							const data_row_sheet = XLSX.utils.sheet_to_json(worksheet_customer, {
								range: range_row_sheet,
							});
							// add data if not null
							if (data_row_sheet.length !== 0) {
								data_customer_object.push(data_row_sheet);
							}
						}
					});
					// END calculate data sheet customer

					resolve({
						customer: data_customer_object,
						customer_opt: data_customer_options.at(0),
						respond: data_respond,
					});
				};
			} catch (e) {
				setLoading(false);
				reject(new Error({ error: true }));
			}
		});
		return newResponse;
	};

	const readExcelPreview = (file) => {
		return new Promise((resolve, reject) => {
			try {
				const fileReader = new FileReader();

				fileReader.readAsArrayBuffer(file);

				fileReader.onload = (e) => {
					const bufferArray = e.target.result;

					const workbook = XLSX.readFile(bufferArray, { type: 'buffer' });

					let _data_excel = {};
					const _sheet_name = [];

					workbook.SheetNames.forEach((sheet) => {
						const worksheet = workbook.Sheets[sheet];
						const _data_worksheet = XLSX.utils.sheet_to_json(worksheet, {
							header: 1,
							defval: '',
						});
						const _columns = [];
						_data_worksheet.at(0).forEach((item, index) => {
							_columns.push(String.fromCharCode('A'.charCodeAt(0) + index));
						});
						const _new_data = [_columns].concat(_data_worksheet);
						const _new_worksheet = XLSX.utils.aoa_to_sheet(_new_data);
						const _new_data_worksheet = XLSX.utils.sheet_to_json(_new_worksheet);

						_data_excel = {
							..._data_excel,
							[sheet]: _new_data_worksheet,
						};
						_sheet_name.push({
							name: sheet,
						});
					});

					resolve({
						data_excel: _data_excel,
						sheets: _sheet_name,
					});
				};
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
	};

	const writeExcel = async () => {
		setLoadingDownload(true);
		try {
			// proc
			const workbook = new ExcelJS.Workbook();
			const worksheet = workbook.addWorksheet(data_excel.sheets.customer.name);

			const data = [
				[],
				// header 1
				[
					'Last Update : ',
					moment().format('MMM DD YYYY'),
					'Note: add a blank line before adding the second new data and so on.',
				],
				// header 2
				[
					'CUSTOMER',
					'COUNTRY',
					'CONTACT',
					'POSITION',
					'EMAIL',
					'T.O.P',
					'PRODUCT',
					'PACKING',
					'FOB',
					moment().add(-1, 'years').format('YYYY'),
					moment().format('YYYY'),
					'Full Potential',
					'POTENTIAL SHARE',
					'',
					moment().format('YYYY'),
					moment().format('YYYY').concat(' ', 'SHARE'),
					'REMARKS',
					'Commercial Lost',
					'Sales Person',
				],
				// header 3
				[
					'NAME',
					'',
					'PERSON',
					'',
					'PHONE',
					'',
					'GRADE',
					'SIZE (KG)',
					'PRICE/KG',
					'QTY (MT)',
					'',
					'',
					'Per Month',
					'Per year',
				],
				// default value
				[
					'text',
					'text',
					'text',
					'text',
					'text',
					'text',
					'text',
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					'text',
					0,
					'LNK000XXX',
				],
			];

			worksheet.addRows(data);

			worksheet.insertRow(23, ['end data']);

			for (let indexRow = 3; indexRow < 25; indexRow += 1) {
				data.at(2).forEach((item, indexCol) => {
					const col_char = String.fromCharCode('A'.charCodeAt(0) + indexCol).concat(
						indexRow,
					);
					const cell = worksheet.getCell(col_char);
					cell.alignment = { vertical: 'middle', horizontal: 'center' };
					cell.font = { size: 11 };
					// color header
					if (indexRow === 3 || indexRow === 4) {
						cell.font = { bold: true };
						cell.fill = {
							type: 'pattern',
							pattern: 'solid',
							bgColor: { argb: 'FFFFFF00' },
							fgColor: { argb: 'FFFFFF00' },
						};
					}
					// color commercial lost & sales person
					if (indexCol >= data.at(2).length - 2) {
						cell.fill = {
							type: 'pattern',
							pattern: 'solid',
							fgColor: { argb: 'FFE4DFEC' },
						};
					}
					// color different new data
					if (indexRow === 5 || indexRow === 12 || indexRow === 19) {
						cell.fill = {
							type: 'pattern',
							pattern: 'solid',
							fgColor: { argb: 'FF92CDDC' },
						};
					}
				});
			}

			worksheet.getCell('A5').note = 'Text value and required';
			worksheet.getCell('B5').note = 'Text value and required';
			worksheet.getCell('C5').note = 'Text value and required';
			worksheet.getCell('D5').note = 'Text value';
			worksheet.getCell('E5').note = 'Text value';
			worksheet.getCell('F5').note = 'Text value';
			worksheet.getCell('G5').note = 'Text value';
			worksheet.getCell('H5').note = 'Number value';
			worksheet.getCell('I5').note = 'Number value';
			worksheet.getCell('J5').note = 'Number value';
			worksheet.getCell('K5').note = 'Number value';
			worksheet.getCell('L5').note = 'Number value';
			worksheet.getCell('M5').note = 'Number value';
			worksheet.getCell('N5').note = 'Number value';
			worksheet.getCell('O5').note = 'Number value';
			worksheet.getCell('P5').note = 'Number value';
			worksheet.getCell('Q5').note = 'Text value';
			worksheet.getCell('R5').note = 'Number value';
			worksheet.getCell('S5').note = 'NIK Value and required';

			worksheet.mergeCells('M3:N3');
			worksheet.mergeCells('C2:D2');

			worksheet.getColumn(1).width = 60;
			worksheet.getColumn(2).width = 18;
			worksheet.getColumn(3).width = 35;
			worksheet.getColumn(4).width = 32;
			worksheet.getColumn(5).width = 94;
			worksheet.getColumn(6).width = 68;
			worksheet.getColumn(7).width = 28;
			worksheet.getColumn(8).width = 9;
			worksheet.getColumn(9).width = 12;
			worksheet.getColumn(10).width = 9;
			worksheet.getColumn(11).width = 8;
			worksheet.getColumn(12).width = 13;
			worksheet.getColumn(13).width = 17;
			worksheet.getColumn(14).width = 9;
			worksheet.getColumn(15).width = 8;
			worksheet.getColumn(16).width = 14;
			worksheet.getColumn(17).width = 66;
			worksheet.getColumn(18).width = 17;
			worksheet.getColumn(19).width = 14;

			worksheet.getCell('A2').alignment = { vertical: 'middle', horizontal: 'right' };
			worksheet.getCell('B2').alignment = { vertical: 'middle', horizontal: 'right' };
			worksheet.getCell('A2').font = { size: 11, color: { argb: 'FFFF1100' } };
			worksheet.getCell('B2').font = { size: 11, color: { argb: 'FFFF1100' } };

			worksheet.getCell('A23').note =
				"Add data before this 'end data' and add blank line at latest data";
			worksheet.getCell('A23').font = { size: 11, color: { argb: 'FFFF1100' } };

			worksheet.getRow(3).commit();

			const file = await workbook.xlsx.writeBuffer(data_excel.file.name);

			FileDownload(file, data_excel.file.name);
		} catch (e) {
			showNotification('Information', 'Error download template', 'danger');
		} finally {
			setLoadingDownload(false);
		}
	};

	return (
		<Accordion id='accordion-form' activeItemId='acc-item-default' stretch>
			<AccordionItem id='acc-item-form' title={t('form')} icon='AddBox'>
				<div className='row'>
					<div className='col-md-5'>
						<Card shadow='sm' tag='form' noValidate onSubmit={formikBulk.handleSubmit}>
							<CardBody>
								<FormGroup id='form_file' label='File' className='mt-3'>
									<Input
										id='form_file'
										type='file'
										onChange={onCustomChange}
										onBlur={formikBulk.handleBlur}
										value={formikBulk.values.form_file}
										isValid={formikBulk.isValid}
										isTouched={formikBulk.touched.form_file}
										invalidFeedback={formikBulk.errors.form_file}
									/>
								</FormGroup>
							</CardBody>
							<CardFooter>
								<CardFooterLeft>
									{loadingDownload ? (
										<Button color='info' isLight={darkModeStatus}>
											<Spinner isSmall inButton />
											Loading...
										</Button>
									) : (
										<Button
											icon='Download'
											color='info'
											type='button'
											isLight={darkModeStatus}
											onClick={writeExcel}>
											Template
										</Button>
									)}
								</CardFooterLeft>
								<CardFooterRight>
									{loading ? (
										<Button color='success' isLight={darkModeStatus}>
											<Spinner isSmall inButton />
											Loading...
										</Button>
									) : (
										<Button
											icon='Save'
											color='success'
											type='submit'
											isLight={darkModeStatus}>
											Submit
										</Button>
									)}
								</CardFooterRight>
							</CardFooter>
						</Card>
					</div>
				</div>
				{preview && dataPreview && (
					<div className='row'>
						<div className='col-md-12'>
							<TablePreview data={dataPreview} />
						</div>
					</div>
				)}
			</AccordionItem>
		</Accordion>
	);
};

FormDataCustomer.propTypes = {
	initialValues: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	loading: PropTypes.bool,
	setLoading: PropTypes.func,
	handleCustomSubmitBulk: PropTypes.func,
};
FormDataCustomer.defaultProps = {
	initialValues: null,
	loading: false,
	setLoading: null,
	handleCustomSubmitBulk: null,
};

const FormDataCustomerDetailModal = ({ initialValues }) => {
	const { t } = useTranslation('crud');
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setIsOpen] = useState(false);

	const {
		customer_name,
		country,
		top,
		comercial_lost,
		sales_person,
		contact_person,
		contact_detail,
		details,
		qty_year_1,
		qty_year_2,
	} = initialValues;

	return (
		<>
			<Button
				icon='GridView'
				color='info'
				type='button'
				isLight={darkModeStatus}
				onClick={() => {
					setIsOpen(true);
				}}
			/>

			<Modal
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='xl'
				titleId='modal-crud'
				isScrollable>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-crud'>View Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					{/* GENERAL INFORMATION */}
					<div className='row'>
						<div className='col-md-12'>
							<Card shadow='sm'>
								<CardHeader borderSize={1} size='sm'>
									<CardLabel icon='Assignment'>
										<CardTitle>{t('general_information')}</CardTitle>
									</CardLabel>
								</CardHeader>
								<CardBody>
									<FormGroup
										id='customer_name'
										label='Customer'
										className='mt-3 col-md-12'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input value={customer_name} readOnly />
									</FormGroup>
									<FormGroup
										id='country'
										label='Country'
										className='mt-3 col-md-12'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input value={country} readOnly />
									</FormGroup>
									<FormGroup
										id='top'
										label='T.O.P'
										className='mt-3 col-md-12'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input value={top} readOnly />
									</FormGroup>
									<FormGroup
										id='comercial_lost'
										label='Comercial Lost'
										className='mt-3 col-md-12'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input value={comercial_lost} readOnly />
									</FormGroup>
									<FormGroup
										id='sales_person'
										label='Sales Person'
										className='mt-3 col-md-12'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input value={sales_person} readOnly />
									</FormGroup>
								</CardBody>
							</Card>
						</div>
					</div>
					{/* CONTACT */}
					<div className='row'>
						{/* CONTACT PERSON */}
						<div className='col-md-8'>
							<Card shadow='sm' stretch>
								<CardHeader borderSize={1} size='sm'>
									<CardLabel icon='Contacts'>
										<CardTitle>{t('contact_person')}</CardTitle>
									</CardLabel>
								</CardHeader>
								<CardBody className='table-responsive'>
									<table className='table table-modern'>
										<thead>
											<tr>
												<th scope='col' style={{ whiteSpace: 'nowrap' }}>
													No
												</th>
												<th scope='col' style={{ whiteSpace: 'nowrap' }}>
													Name
												</th>
												<th scope='col' style={{ whiteSpace: 'nowrap' }}>
													Position
												</th>
												<th scope='col' style={{ whiteSpace: 'nowrap' }}>
													Email
												</th>
											</tr>
										</thead>
										<tbody>
											{contact_person.map((item, index) => (
												<tr key={'person-row-'.concat(index)}>
													<td
														key={'person-col-'.concat(index, 1)}
														style={{ whiteSpace: 'nowrap' }}>
														{index + 1}
													</td>
													<td
														key={'person-col-'.concat(index, 2)}
														style={{ whiteSpace: 'nowrap' }}>
														{item.name}
													</td>
													<td
														key={'person-col-'.concat(index, 3)}
														style={{ whiteSpace: 'nowrap' }}>
														{item.position}
													</td>
													<td
														key={'person-col-'.concat(index, 4)}
														style={{ whiteSpace: 'nowrap' }}>
														{item.email}
													</td>
												</tr>
											))}
										</tbody>
									</table>
								</CardBody>
							</Card>
						</div>
						{/* CONTACT DETAIL */}
						<div className='col-md-4'>
							<Card shadow='sm' style={{ height: '16rem' }}>
								<CardHeader borderSize={1} size='sm'>
									<CardLabel icon='ManageSearch'>
										<CardTitle>{t('contact_detail')}</CardTitle>
									</CardLabel>
								</CardHeader>
								<CardBody isScrollable>
									<ListGroup isFlush>
										{contact_detail.map((item, index) => (
											<ListGroupItem key={'item-'.concat(index)}>
												{item}
											</ListGroupItem>
										))}
									</ListGroup>
								</CardBody>
							</Card>
						</div>
					</div>
					{/* DETAIL PRODUCT */}
					<div className='row'>
						<div className='col-md-12'>
							<Card shadow='sm'>
								<CardHeader borderSize={1} size='sm'>
									<CardLabel icon='LibraryBooks'>
										<CardTitle>{t('detail_product')}</CardTitle>
									</CardLabel>
								</CardHeader>
								<CardBody>
									<div className='table-responsive'>
										<table className='table table-modern'>
											<thead>
												<tr>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														No
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														Product Grade
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														Packing Size (Kg)
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														FOB Price
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														QTY Year {qty_year_1}
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														QTY Year {qty_year_2}
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														Full Potential
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														Potential Per Month
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														Potential Per Year
													</th>
													<th
														scope='col'
														style={{ whiteSpace: 'nowrap' }}>
														Remark
													</th>
												</tr>
											</thead>
											<tbody>
												{details.map((item, index) => (
													<tr key={'detail-row-'.concat(index)}>
														<td
															key={'detail-col-'.concat(index, 1)}
															style={{ whiteSpace: 'nowrap' }}>
															{index + 1}
														</td>
														<td
															key={'detail-col-'.concat(index, 2)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.product_grade}
														</td>
														<td
															key={'detail-col-'.concat(index, 3)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.packing_size}
														</td>
														<td
															key={'detail-col-'.concat(index, 4)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.fob_price}
														</td>
														<td
															key={'detail-col-'.concat(index, 5)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.qty_year_1}
														</td>
														<td
															key={'detail-col-'.concat(index, 6)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.qty_year_2}
														</td>
														<td
															key={'detail-col-'.concat(index, 7)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.full_potential}
														</td>
														<td
															key={'detail-col-'.concat(index, 8)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.potential_per_month}
														</td>
														<td
															key={'detail-col-'.concat(index, 9)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.potential_per_year}
														</td>
														<td
															key={'detail-col-'.concat(index, 10)}
															style={{ whiteSpace: 'nowrap' }}>
															{item.remarks}
														</td>
													</tr>
												))}
											</tbody>
										</table>
									</div>
								</CardBody>
							</Card>
						</div>
					</div>
				</ModalBody>
			</Modal>
		</>
	);
};

FormDataCustomerDetailModal.propTypes = {
	initialValues: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
};
FormDataCustomerDetailModal.defaultProps = {
	initialValues: null,
};

const FormDataCustomerBulkModal = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setIsOpen] = useState(false);
	const { username } = getRequester();

	const { department, version } = initialValues;

	const [loading, setLoading] = useState(false);
	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const fetchDataCustomer = (page, newPerPage) => {
		setLoading(true);
		const query = `page=${page}&sizePerPage=${newPerPage}&department=${department}&version_ots=${version}&requester=${username}`;
		DataCustomerModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch((err) => {
				showNotification('Information', err, 'danger');
			})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<>
			<Button
				icon='GridView'
				color='info'
				type='button'
				isLight={darkModeStatus}
				onClick={() => {
					setIsOpen(true);
					fetchDataCustomer(curPage, perPage);
				}}
			/>

			<Modal
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='xl'
				titleId='modal-crud'
				isScrollable>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-crud'>View Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<TableDataCustomer
						data={data}
						totalRows={totalRows}
						perPage={perPage}
						curPage={curPage}
						loading={loading}
						fetchData={fetchDataCustomer}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormDataCustomerBulkModal.propTypes = {
	initialValues: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
};
FormDataCustomerBulkModal.defaultProps = {
	initialValues: null,
};

const TableDataCustomer = ({ data, totalRows, perPage, loading, fetchData, isDetail }) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Customer Name',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'Country',
				selector: (row) => row.country,
				sortable: true,
			},
			{
				name: 'Version',
				selector: (row) => row.version,
				sortable: true,
			},
			{
				name: 'Created',
				selector: (row) => row.created_at,
				format: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
		],
		[],
	);

	const columns_action = useMemo(
		() => [
			{
				name: 'Customer Name',
				selector: (row) => row.customer_name,
				sortable: true,
			},
			{
				name: 'Country',
				selector: (row) => row.country,
				sortable: true,
			},
			{
				name: 'Version',
				selector: (row) => row.version,
				sortable: true,
			},
			{
				name: 'Created',
				selector: (row) => row.created_at,
				format: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
			{
				name: 'Actions',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					return <FormDataCustomerDetailModal initialValues={row} />;
				},
			},
		],
		[],
	);

	const handlePageChange = (page) => {
		fetchData(page, perPage);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		return fetchData(page, newPerPage);
	};

	return (
		<DarkDataTable
			columns={isDetail ? columns_action : columns}
			data={data}
			fixedHeader
			responsive
			pagination
			paginationServer
			progressPending={loading}
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableDataCustomer.propTypes = {
	data: PropTypes.oneOfType([PropTypes.instanceOf(Array)]),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	loading: PropTypes.bool,
	isDetail: PropTypes.bool,
	fetchData: PropTypes.func,
};
TableDataCustomer.defaultProps = {
	data: [],
	totalRows: 0,
	perPage: 10,
	loading: false,
	isDetail: false,
	fetchData: null,
};

const TableDataCustomerBulk = ({ data, totalRows, perPage, loading, fetchData }) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Version',
				selector: (row) => row.version,
				sortable: true,
			},
			{
				name: 'Created',
				selector: (row) => row.created_at,
				format: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
			{
				name: 'Actions',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					return <FormDataCustomerBulkModal initialValues={row} />;
				},
			},
		],
		[],
	);

	const handlePageChange = (page) => {
		fetchData(page, perPage);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		return fetchData(page, newPerPage);
	};

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			paginationServer
			progressPending={loading}
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableDataCustomerBulk.propTypes = {
	data: PropTypes.oneOfType([PropTypes.instanceOf(Array)]),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	loading: PropTypes.bool,
	fetchData: PropTypes.func,
};
TableDataCustomerBulk.defaultProps = {
	data: [],
	totalRows: 0,
	perPage: 10,
	loading: false,
	fetchData: null,
};

const DataCustomer = () => {
	const { t } = useTranslation('crud');
	const [title] = useState({ title: 'Data Customer' });
	const { username, department } = getRequester();

	const [loadingImport, setLoadingImport] = useState(false);
	// data customer
	const [loadingData, setLoadingData] = useState(false);
	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);
	// data customer bulk
	const [loadingBulk, setLoadingBulk] = useState(false);
	const [dataBulk, setDataBulk] = useState([]);
	const [totalRowsBulk, setTotalRowsBulk] = useState(0);
	const [perPageBulk, setPerPageBulk] = useState(10);
	const [curPageBulk, setCurPageBulk] = useState(1);

	const data_excel = {
		sheets: {
			customer: {
				name: 'Customer',
				columns_name: [
					'customer_name',
					'country',
					'contact_person',
					'position',
					'email_phone',
					'top',
					'product_grade',
					'packing_size',
					'fob_price',
					'qty_year_1',
					'qty_year_2',
					'full_potential',
					'potential_per_month',
					'potential_per_year',
					'share_year_1',
					'share_year_2',
					'remarks',
					'comercial_lost',
					'sales_person',
				],
			},
			respond: {
				name: 'Respond',
				columns_name: ['company', 'country', 'latest_status', 'pic', 'email', 'phone'],
			},
		},
		file: {
			name: 'template_customer_information.xlsx',
		},
	};

	const initialValues = {
		requester: username,
		department,
		form_file: '',
		data_excel,
	};

	const createSubmit = (values) => {
		handleSubmit(values).then(() => {
			fetchDataCustomerBulk(curPageBulk, perPageBulk);
			fetchDataCustomer(curPage, perPageBulk);
		});
	};

	const createSubmitBulk = (values) => {
		handleSubmitBulk(values)
			.then(() => {
				fetchDataCustomerBulk(curPageBulk, perPageBulk);
				fetchDataCustomer(curPage, perPageBulk);
			})
			.finally(() => {
				setLoadingImport(false);
			});
	};

	useEffect(() => {
		fetchDataCustomerBulk(1, 10);
		fetchDataCustomer(1, 10);

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchDataCustomer = (page, newPerPage) => {
		setLoadingData(true);
		const query = `page=${page}&sizePerPage=${newPerPage}&department=${department}&requester=${username}`;
		DataCustomerModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.finally(() => {
				setLoadingData(false);
			});
	};

	const fetchDataCustomerBulk = (page, newPerPage) => {
		setLoadingBulk(true);
		const query = `page=${page}&sizePerPage=${newPerPage}&department=${department}&requester=${username}`;
		DataCustomerModule.readBulk(query)
			.then((response) => {
				setDataBulk(response.foundData);
				setTotalRowsBulk(response.countData);
				setCurPageBulk(response.currentPage);
				setPerPageBulk(newPerPage);
			})
			.finally(() => {
				setLoadingBulk(false);
			});
	};

	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<FormDataCustomer
							initialValues={initialValues}
							loading={loadingImport}
							setLoading={setLoadingImport}
							handleCustomSubmit={createSubmit}
							handleCustomSubmitBulk={createSubmitBulk}
						/>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History'>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<Card stretch hasTab borderSize={0} shadow='none'>
							<CardTabItem id='data_customer' title={t('data_customer')}>
								<TableDataCustomer
									data={data}
									totalRows={totalRows}
									perPage={perPage}
									curPage={curPage}
									loading={loadingData}
									isDetail
									fetchData={fetchDataCustomer}
								/>
							</CardTabItem>
							<CardTabItem id='data_bulk' title={t('data_bulk')}>
								<TableDataCustomerBulk
									data={dataBulk}
									totalRows={totalRowsBulk}
									perPage={perPageBulk}
									curPage={curPageBulk}
									loading={loadingBulk}
									fetchData={fetchDataCustomerBulk}
								/>
							</CardTabItem>
						</Card>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

DataCustomer.propTypes = {};
DataCustomer.defaultProps = {};

export default DataCustomer;
