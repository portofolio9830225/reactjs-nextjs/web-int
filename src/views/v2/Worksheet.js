import React from 'react';
import PropTypes from 'prop-types';
import * as XLSX from 'xlsx';
import moment from 'moment';
import ExcelJS from 'exceljs';
import { getColumnName } from '../../helpers/helpers';
import Card, { CardTabItem } from '../../components/bootstrap/Card';

export const FORMAT_EXCEL = {
	MASTER_CUSTOMER: {
		name: 'Master Customer',
		column: [
			{ name: 'CUSTOMER', key: 'customer' },
			{ name: 'COUNTRY', key: 'country' },
			{ name: 'EMAIL', key: 'email' },
			{ name: 'ADDRESS', key: 'address' },
			{ name: 'TOP', key: 'top' },
		],
	},
	MASTER_PIC: {
		name: 'PIC',
		column: [
			{ name: 'CUSTOMER', key: 'customer' },
			{ name: 'NAME', key: 'name' },
			{ name: 'POSITION', key: 'position' },
			{ name: 'PHONE', key: 'phone' },
			{ name: 'EMAIL', key: 'email' },
		],
	},
	TRANSAKSI: {
		name: 'Transaksi Penjualan',
		column: [
			{ name: 'CUSTOMER', key: 'customer' },
			{ name: 'DATE', key: 'date' },
			{ name: 'Product', key: 'product' },
			{ name: 'Packaging Size (KG)', key: 'packaging' },
			{ name: 'FOB Price/KG', key: 'fob_price' },
			{ name: 'Qty', key: 'qty' },
			{ name: 'Sales Person', key: 'sales_person' },
		],
	},
};

export const writeFile = async () => {
	try {
		const workbook = new ExcelJS.Workbook();
		const ws_customer = workbook.addWorksheet(FORMAT_EXCEL.MASTER_CUSTOMER.name);
		const ws_pic = workbook.addWorksheet(FORMAT_EXCEL.MASTER_PIC.name);
		const ws_transaksi = workbook.addWorksheet(FORMAT_EXCEL.TRANSAKSI.name);

		// set frozen
		ws_customer.views = [{ state: 'frozen', ySplit: 1, activeCell: 'A1' }];
		ws_pic.views = [{ state: 'frozen', ySplit: 1, activeCell: 'A1' }];
		ws_transaksi.views = [{ state: 'frozen', ySplit: 1, activeCell: 'A1' }];

		const col_customer = FORMAT_EXCEL.MASTER_CUSTOMER.column.map((item) => item.name);
		const col_pic = FORMAT_EXCEL.MASTER_PIC.column.map((item) => item.name);
		const col_transaksi = FORMAT_EXCEL.TRANSAKSI.column.map((item) => item.name);

		ws_customer.addRows([col_customer]);
		ws_pic.addRows([col_pic]);
		ws_transaksi.addRows([col_transaksi]);

		const MAX_ROWS = 1000;

		// SHEET : CUSTOMER
		// add styles
		for (let i = 0; i < col_customer.length; i += 1) {
			const column = getColumnName(i);

			const _column = ws_customer.getColumn(column);
			_column.width = 15;

			const cell = `${column}1`;
			ws_customer.getCell(cell).fill = {
				type: 'pattern',
				pattern: 'solid',
				fgColor: { argb: 'FFFFFF00' },
			};
		}

		// SHEET : PIC
		// add styles
		for (let i = 0; i < col_pic.length; i += 1) {
			const column = getColumnName(i);

			const _column = ws_pic.getColumn(column);
			_column.width = 15;

			const cell = `${column}1`;
			ws_pic.getCell(cell).fill = {
				type: 'pattern',
				pattern: 'solid',
				fgColor: { argb: 'FFFFFF00' },
			};
		}

		// add validations - id_customer
		for (let i = 2; i <= MAX_ROWS; i += 1) {
			ws_pic.getCell(`A${i}`).dataValidation = {
				type: 'list',
				allowBlank: true,
				showErrorMessage: true,
				error: `'Data tidak ada dalam sheet '${FORMAT_EXCEL.MASTER_CUSTOMER.name}'`,
				formulae: [`='${FORMAT_EXCEL.MASTER_CUSTOMER.name}'!$A$2:$A$${MAX_ROWS}`],
			};
		}

		// SHEET : TRANSACTION
		// add styles
		for (let i = 0; i < col_transaksi.length; i += 1) {
			const column = getColumnName(i);

			const _column = ws_transaksi.getColumn(column);
			_column.width = 15;

			const cell = `${column}1`;
			ws_transaksi.getCell(cell).fill = {
				type: 'pattern',
				pattern: 'solid',
				fgColor: { argb: 'FFFFFF00' },
			};
		}

		// add validations - id_customer
		for (let i = 2; i <= MAX_ROWS; i += 1) {
			ws_transaksi.getCell(`A${i}`).dataValidation = {
				type: 'list',
				allowBlank: true,
				showErrorMessage: true,
				error: `'Data tidak ada dalam sheet '${FORMAT_EXCEL.MASTER_CUSTOMER.name}'`,
				formulae: [`='${FORMAT_EXCEL.MASTER_CUSTOMER.name}'!$A$2:$A$${MAX_ROWS}`],
			};
		}

		// add validations - date
		for (let i = 2; i <= MAX_ROWS; i += 1) {
			ws_transaksi.getCell(`B${i}`).numFmt = 'yyyy-mm-dd';
			ws_transaksi.getCell(`B${i}`).dataValidation = {
				type: 'date',
				allowBlank: true,
				showErrorMessage: true,
				error: 'Format date YYYY-MM-DD',
				formulae: [
					new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay()),
				],
			};
		}

		const datetime = moment(new Date()).format('YYYYMMDD_HHmm');

		const fileName = `template_${datetime}.xlsx`;

		const fileExcel = await workbook.xlsx.writeBuffer(fileName);

		return Promise.resolve({ fileExcel, fileName });
	} catch (error) {
		return Promise.reject(new Error(error.message));
	}
};

export const readFileFormat = (file) => {
	return new Promise((resolve, reject) => {
		try {
			const fileReader = new FileReader();
			fileReader.readAsArrayBuffer(file);

			fileReader.onload = (e) => {
				const bufferString = e.target.result;
				const workbook = XLSX.read(bufferString, { type: 'buffer' });

				const ws_customer = workbook.Sheets[FORMAT_EXCEL.MASTER_CUSTOMER.name];
				const ws_pic = workbook.Sheets[FORMAT_EXCEL.MASTER_PIC.name];
				const ws_transaksi = workbook.Sheets[FORMAT_EXCEL.TRANSAKSI.name];

				if (!ws_customer || !ws_pic || !ws_transaksi) {
					reject(new Error('Invalid workbook'));
					return;
				}

				const col_customer = FORMAT_EXCEL.MASTER_CUSTOMER.column.map((item) => item.key);
				const col_pic = FORMAT_EXCEL.MASTER_PIC.column.map((item) => item.key);
				const col_transaksi = FORMAT_EXCEL.TRANSAKSI.column.map((item) => item.key);

				XLSX.utils.sheet_add_aoa(ws_customer, [col_customer], { origin: 'A1' });
				XLSX.utils.sheet_add_aoa(ws_pic, [col_pic], { origin: 'A1' });
				XLSX.utils.sheet_add_aoa(ws_transaksi, [col_transaksi], { origin: 'A1' });

				const data_customer = XLSX.utils.sheet_to_json(ws_customer);
				const data_pic = XLSX.utils.sheet_to_json(ws_pic);
				const data_transaksi = XLSX.utils.sheet_to_json(ws_transaksi);

				resolve({ data_customer, data_pic, data_transaksi });
			};
		} catch (error) {
			reject(new Error(error.message));
		}
	});
};

export const readFile = (file) => {
	return new Promise((resolve, reject) => {
		try {
			const fileReader = new FileReader();
			fileReader.readAsArrayBuffer(file);

			fileReader.onload = (e) => {
				const binaryString = e.target.result;
				const workbook = XLSX.read(binaryString, { type: 'buffer' });

				const worksheets = workbook.SheetNames.map((item) => {
					const worksheet = workbook.Sheets[item];
					return {
						sheetName: item,
						workSheet: XLSX.utils.sheet_to_json(worksheet, {
							header: 1,
							defval: '',
						}),
					};
				});
				resolve(worksheets);
			};
		} catch (e) {
			reject(new Error(e.message));
		}
	});
};

const WorksheetContent = ({ data }) => {
	return (
		<div className='table-responsive' style={{ maxHeight: '500px' }}>
			<table className='table table-sm table-striped table-bordered'>
				<thead>
					<tr>
						<th style={{ textAlign: 'center', whiteSpace: 'nowrap' }}>#</th>
						{data.length > 0 &&
							data[0]?.map((item, index) => (
								<th
									key={'th'.concat(index)}
									style={{ textAlign: 'center', whiteSpace: 'nowrap' }}>
									{getColumnName(index)}
								</th>
							))}
					</tr>
				</thead>
				<tbody>
					{data?.map((itemRow, indexRow) => (
						<tr key={'item-row'.concat(indexRow)}>
							<th key={'item-row'.concat(indexRow, 'item-col')} scope='col'>
								{indexRow + 1}
							</th>
							{itemRow?.map((itemCol, indexCol) => (
								<td
									key={'item-row'.concat(indexRow, 'item-col', indexCol)}
									style={{ whiteSpace: 'nowrap' }}>
									{itemCol}
								</td>
							))}
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

WorksheetContent.propTypes = {
	data: PropTypes.instanceOf(Array),
};
WorksheetContent.defaultProps = {
	data: [],
};

const Worksheet = ({ data, ...props }) => {
	return (
		// eslint-disable-next-line react/jsx-props-no-spreading
		<Card shadow='sm' hasTab {...props}>
			{data?.map((item, index) => (
				<CardTabItem
					key={'card-item-'.concat(index)}
					id={item.sheetName}
					title={item.sheetName}>
					<WorksheetContent data={item.workSheet} />
				</CardTabItem>
			))}
		</Card>
	);
};

Worksheet.propTypes = {
	data: PropTypes.instanceOf(Array),
};
Worksheet.defaultProps = {
	data: [],
};

export default Worksheet;
