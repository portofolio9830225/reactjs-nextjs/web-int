import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useFormik } from 'formik';
import Swal from 'sweetalert2';
import moment from 'moment';
import classNames from 'classnames';
import FileDownload from 'js-file-download';
import { useTranslation } from 'react-i18next';
import Button from '../../components/bootstrap/Button';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../components/bootstrap/Card';
import useDarkMode from '../../hooks/useDarkMode';
import Page from '../../layout/Page/Page';
import PageWrapper from '../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../pages/common/Headers/PageLayoutHeader';
import FormGroup from '../../components/bootstrap/forms/FormGroup';
import Checks from '../../components/bootstrap/forms/Checks';
import Input from '../../components/bootstrap/forms/Input';
import Tooltips from '../../components/bootstrap/Tooltips';
import Worksheet, { readFile, readFileFormat, writeFile } from './Worksheet';
import Spinner from '../../components/bootstrap/Spinner';
import showNotification from '../../components/extras/showNotification';
import { getRequester } from '../../helpers/helpers';
import CustomerModule from '../../modules/v2/CustomerModule';
import Accordion, { AccordionItem } from '../../components/bootstrap/Accordion';
import DarkDataTable from '../../components/DarkDataTable';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../../components/bootstrap/Modal';
import TransactionComponent from './TransactionComponent';
import PicComponent from './PicComponent';

const handleCreateBulk = (values) => {
	return new Promise((resolve, reject) => {
		try {
			CustomerModule.createBulk(values)
				.then(() => {
					Swal.fire('Information!', 'Data has been saved successfully', 'success');
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Swal.fire('Warning!', err, 'error');
					reject(new Error(err));
				});
		} catch (error) {
			reject(new Error(error.message));
		}
	});
};

const FormCustom = ({ initialValues, isReadOnly }) => {
	const formik = useFormik({
		initialValues: {
			...initialValues,
			country: initialValues?.country ?? '',
			customer: initialValues?.customer ?? '',
			email: initialValues?.email ?? '',
			address: initialValues?.address ?? '',
			pic: initialValues?.pic ?? [],
			transaksi: initialValues?.transaksi ?? [],
		},
		validate: () => {
			const errors = {};

			return errors;
		},
		onReset: () => {},
		onSubmit: () => {},
	});

	return (
		<div className={classNames('p-1')} tag='form' noValidate onSubmit={formik.handleSubmit}>
			<div className={classNames('row', 'py-1')}>
				<div className={classNames('col-md-12')}>
					<Card shadow='sm'>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='Assignment' iconColor='info'>
								<CardTitle>General Information</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className={classNames('row', 'p-1')}>
								<div className={classNames('col-md-6', 'py-2')}>
									<FormGroup id='country' label='Country' isFloating>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.country}
											isValid={formik.isValid}
											isTouched={formik.touched.country}
											invalidFeedback={formik.errors.country}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>

								<div className={classNames('col-md-6', 'py-2')}>
									<FormGroup id='customer' label='Customer' isFloating>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.customer}
											isValid={formik.isValid}
											isTouched={formik.touched.customer}
											invalidFeedback={formik.errors.customer}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>

								<div className={classNames('col-md-6', 'py-2')}>
									<FormGroup id='email' label='Email' isFloating>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.email}
											isValid={formik.isValid}
											isTouched={formik.touched.email}
											invalidFeedback={formik.errors.email}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>

								<div className={classNames('col-md-6', 'py-2')}>
									<FormGroup id='address' label='Address' isFloating>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.address}
											isValid={formik.isValid}
											isTouched={formik.touched.address}
											invalidFeedback={formik.errors.address}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>

								<div className={classNames('col-md-6', 'py-2')}>
									<FormGroup id='top' label='TOP' isFloating>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.top}
											isValid={formik.isValid}
											isTouched={formik.touched.top}
											invalidFeedback={formik.errors.top}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>
							</div>
						</CardBody>
					</Card>
				</div>

				<div className={classNames('col-md-12')}>
					<Card shadow='sm'>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='Contacts' iconColor='info'>
								<CardTitle>Contact</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<PicComponent
								id_customer={initialValues?._id}
								data={formik.values.pic}
								onChange={(e) => {
									formik.setFieldValue('pic', e);
								}}
							/>
						</CardBody>
					</Card>
				</div>

				<div className={classNames('col-md-12')}>
					<Card shadow='sm'>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='LibraryBooks' iconColor='info'>
								<CardTitle>Transaction</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<TransactionComponent
								id_customer={initialValues?._id}
								data={formik.values.transaksi}
								onChange={(e) => {
									formik.setFieldValue('transaksi', e);
								}}
							/>
						</CardBody>
					</Card>
				</div>
			</div>
		</div>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	isReadOnly: PropTypes.bool,
};
FormCustom.defaultProps = {
	initialValues: null,
	isReadOnly: false,
};

const FormCustomAdd = ({ initialValues, options, handleCustomSubmit }) => {
	const { darkModeStatus } = useDarkMode();
	const { department, username } = getRequester();

	const [isLoading, setIsLoading] = useState(false);
	const [isLoadingGenerate, setIsLoadingGenerate] = useState(false);

	const formik = useFormik({
		initialValues: {
			...initialValues,
			data: initialValues?.data ?? [],
			file: initialValues?.file ?? '',
			file_object: initialValues?.file_object ?? null,
		},
		validate: (values) => {
			const errors = {};

			if (!values.file) {
				errors.file = 'Required';
			}

			return errors;
		},
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						setIsLoading(true);

						readFileFormat(values.file_object)
							.then((response) => {
								const new_values = {};
								new_values.requester = username;
								new_values.department = department;
								new_values.data_customer = response.data_customer;
								new_values.data_pic = response.data_pic;
								new_values.data_transaksi = response.data_transaksi;

								handleCustomSubmit(new_values, options, resetForm, setIsLoading);
							})
							.catch((err) => {
								showNotification('Information!', err.message, 'danger');
								setIsLoading(false);
							})
							.finally(() => {});
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
			} finally {
				setSubmitting(false);
			}
		},
	});

	const handleChange = (e) => {
		formik.handleChange(e);

		if (e?.target?.files?.length) {
			const fileObject = e.target.files[0];
			formik.setFieldValue('file_object', fileObject);

			readFile(fileObject)
				.then((response) => {
					formik.setFieldValue('data', response);
				})
				.catch(() => {
					formik.setFieldValue('data', []);
				})
				.finally(() => {});
		} else {
			formik.setFieldValue('file_object', null);
		}
	};

	const handleGenerate = () => {
		setIsLoadingGenerate(true);

		writeFile()
			.then((response) => {
				FileDownload(response.fileExcel, response.fileName);
			})
			.catch(() => {})
			.finally(() => {
				setIsLoadingGenerate(false);
			});
	};

	return (
		<div className={classNames('row')} tag='form' noValidate onSubmit={formik.handleSubmit}>
			<div className={classNames('col-md-12', 'py-2')}>
				<FormGroup id='file'>
					<Input
						type='file'
						onChange={handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.file}
						isValid={formik.isValid}
						isTouched={formik.touched.file}
						invalidFeedback={formik.errors.file}
						accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
					/>
				</FormGroup>
			</div>

			{formik.values.data.length !== 0 && (
				<div className={classNames('col-md-12', 'py-2')}>
					<Worksheet data={formik.values.data} />
				</div>
			)}

			<div className={classNames('col-md-12', 'py-2')}>
				{isLoadingGenerate ? (
					<Button color='info' className={classNames('float-start', 'me-1')}>
						<Spinner isSmall inButton /> Loading
					</Button>
				) : (
					<Button
						icon='CloudDownload'
						color='info'
						type='button'
						onClick={handleGenerate}
						className={classNames('float-start', 'me-1')}
						isLight={darkModeStatus}>
						Download Template
					</Button>
				)}

				{isLoading ? (
					<Button color='success' className={classNames('float-end', 'ms-1')}>
						<Spinner isSmall inButton /> Loading
					</Button>
				) : (
					<Button
						icon='CloudUpload'
						color='success'
						type='button'
						onClick={formik.handleSubmit}
						className={classNames('float-end', 'ms-1')}
						isLight={darkModeStatus}>
						Upload Data
					</Button>
				)}

				<Button
					icon='Clear'
					color='danger'
					type='button'
					onClick={formik.handleReset}
					className={classNames('float-end', 'ms-1')}
					isLight={darkModeStatus}>
					Clear
				</Button>
			</div>
		</div>
	);
};

FormCustomAdd.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	options: PropTypes.instanceOf(Object),
	handleCustomSubmit: PropTypes.func,
};
FormCustomAdd.defaultProps = {
	initialValues: { data: [], file: '', file_object: null },
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	handleCustomSubmit: () => {},
};

const FormCustomModalView = ({ initialValues, isOpen, setIsOpen, isReadOnly, title }) => {
	return (
		<Modal
			isOpen={isOpen}
			setIsOpen={setIsOpen}
			titleId='modal-crud'
			size='xl'
			isStaticBackdrop>
			<ModalHeader setIsOpen={setIsOpen} className='p-4'>
				<ModalTitle id='modal-crud'>{title}</ModalTitle>
			</ModalHeader>
			<ModalBody>
				<FormCustom initialValues={initialValues} isReadOnly={isReadOnly} />
			</ModalBody>
		</Modal>
	);
};

FormCustomModalView.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	isReadOnly: PropTypes.bool,
	isOpen: PropTypes.bool,
	setIsOpen: PropTypes.func,
	title: PropTypes.string,
};
FormCustomModalView.defaultProps = {
	initialValues: null,
	isReadOnly: false,
	isOpen: false,
	setIsOpen: () => {},
	title: 'modal',
};

const ButtonCustom = ({ initialValues }) => {
	const { darkModeStatus } = useDarkMode();

	const [showModalView, setShowModalView] = useState(false);
	const [modalViewEdit, setModalViewEdit] = useState(false);
	const [modalViewTitle, setModalViewTitle] = useState('modal');

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				onClick={() => {
					setModalViewTitle('View');
					setModalViewEdit(false);
					setShowModalView(true);
				}}
				isLight={darkModeStatus}
				className={classNames('mx-1')}
			/>
			{/* 
			<Button
				icon='Edit'
				color='success'
				type='button'
				onClick={() => {
					setModalViewTitle('Edit');
					setModalViewEdit(true);
					setShowModalView(true);
				}}
				isLight={darkModeStatus}
				className={classNames('mx-1')}
			/> */}

			{/* <Button
				icon='Delete'
				color='danger'
				type='button'
				isLight={darkModeStatus}
				className={classNames('mx-1')}
			/> */}

			<FormCustomModalView
				initialValues={initialValues}
				isOpen={showModalView}
				setIsOpen={setShowModalView}
				title={modalViewTitle}
				isReadOnly={!modalViewEdit}
			/>
		</>
	);
};

ButtonCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
};
ButtonCustom.defaultProps = {
	initialValues: null,
};

const TableCustom = ({ data, totalRows, options, loading, isReset, showHiden, fetchData }) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, options.perPage, options.showAll, options.filter);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = newPerPage === totalRows;
		return fetchData(page, newPerPage, all, options.filter);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'COUNTRY',
				selector: (row) => row.country,
				sortable: true,
				maxWidth: '300px',
			},
			{
				name: 'CUSTOMER',
				selector: (row) => row.customer,
				sortable: true,
				maxWidth: '300px',
			},
			{
				name: 'EMAIL',
				selector: (row) => row.email,
				sortable: true,
				maxWidth: '300px',
			},
			{
				name: 'ADDRESS',
				cell: (row) => row.address,
				maxWidth: '300px',
			},
			{
				name: 'CREATED AT',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD HH:mm'),
				sortable: true,
				maxWidth: '180px',
				omit: !showHiden,
			},
			{
				name: 'CREATED BY',
				selector: (row) => row.created_by,
				sortable: true,
				maxWidth: '180px',
				omit: !showHiden,
			},
			{
				name: 'ACTION',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					return <ButtonCustom initialValues={row} options={options} />;
				},
				maxWidth: '300px',
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[options],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			progressComponent={
				<div className='pt-5'>
					<Spinner color='info' size='8rem' />
				</div>
			}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			paginationResetDefaultPage={isReset}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	loading: PropTypes.bool,
	isReset: PropTypes.bool,
	options: PropTypes.instanceOf(Object),
	showHiden: PropTypes.bool,
	fetchData: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	totalRows: 0,
	loading: false,
	isReset: false,
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	showHiden: true,
	fetchData: () => {},
};

const FilterCustom = ({ options, fetchData }) => {
	const { darkModeStatus } = useDarkMode();

	const [description, setDescription] = useState('');

	const handleFind = () => {
		fetchData(1, options.perPage, options.showAll, { description });
	};

	const handleClear = () => {
		setDescription('');
		fetchData(1, options.perPage, options.showAll, {}, true);
	};

	return (
		<div className={classNames('p-2')}>
			<div className={classNames('row')}>
				<div className={classNames('col-md-4')}>
					<FormGroup id='description' label='Description'>
						<Input
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							placeholder='Country / Customer / Email / Address'
							autoComplete='off'
						/>
					</FormGroup>
				</div>
				<div className={classNames('col-md-3', 'd-flex', 'align-items-end')}>
					<Button
						icon='Search'
						color='success'
						type='button'
						onClick={handleFind}
						className={classNames('mx-1')}
						isLight={darkModeStatus}>
						Find
					</Button>
					<Button
						icon='Clear'
						color='danger'
						type='button'
						onClick={handleClear}
						className={classNames('mx-1')}
						isLight={darkModeStatus}>
						Clear
					</Button>
				</div>
			</div>
		</div>
	);
};

FilterCustom.propTypes = {
	options: PropTypes.instanceOf(Object),
	fetchData: PropTypes.func,
};
FilterCustom.defaultProps = {
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	fetchData: () => {},
};

const DataCustomer = () => {
	const { t } = useTranslation('crud');
	const { username, department } = getRequester();

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [showAll, setShowAll] = useState(false);
	const [loading, setLoading] = useState(false);
	const [filter, setFilter] = useState({});

	const [isReset, setIsReset] = useState(false);

	const [showHiden, setShowHiden] = useState(false);

	const initialValues = { data: [], file: '', file_object: null };

	const createBulkSubmit = (values, options, resetForm, setIsLoading) => {
		handleCreateBulk(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll, options.filter);
				resetForm(initialValues);
			})
			.catch(() => {})
			.finally(() => {
				setIsLoading(false);
			});
	};

	useEffect(() => {
		fetchData(curPage, perPage, showAll, filter);

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, newShowAll, newFilter = {}, reset = false) => {
		setShowAll(newShowAll);
		setFilter(newFilter);
		setLoading(true);
		setIsReset(reset);

		const query = {
			page: newPage,
			sizePerPage: newPerPage,
			showAll: newShowAll,
			requester: username,
			underLevel: true,
			department,
			...newFilter,
		};

		return CustomerModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
				setIsReset(false);
			});
	};

	return (
		<PageWrapper title={t('Data Customer')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<Accordion
							id='accordion-form'
							activeItemId='form'
							shadow='sm'
							color='info'
							stretch>
							<AccordionItem id='form-create' title={t('form')} icon='AddBox'>
								<FormCustomAdd
									initialValues={initialValues}
									handleCustomSubmit={createBulkSubmit}
								/>
							</AccordionItem>
						</Accordion>
					</CardBody>
				</Card>

				<Card stretch>
					<CardHeader borderSize={1} size='sm'>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
						<CardActions>
							<Tooltips title='Show Column Created'>
								<Checks
									type='switch'
									label={showHiden ? 'Show' : 'Hide'}
									checked={showHiden}
									onChange={(e) => setShowHiden(e.target.checked)}
								/>
							</Tooltips>
						</CardActions>
					</CardHeader>
					<CardBody>
						<FilterCustom
							options={{ curPage, perPage, showAll, filter }}
							fetchData={fetchData}
						/>

						<TableCustom
							data={data}
							totalRows={totalRows}
							loading={loading}
							isReset={isReset}
							showHiden={showHiden}
							options={{ curPage, perPage, showAll, filter }}
							fetchData={fetchData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

DataCustomer.propTypes = {};
DataCustomer.defaultProps = {};

export default DataCustomer;
