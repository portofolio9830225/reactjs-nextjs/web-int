import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import PicModule from '../../modules/v2/PicModule';
import Spinner from '../../components/bootstrap/Spinner';

const PicComponent = ({ id_customer, data, onChange }) => {
	const [loadingContact, setLoadingContact] = useState(false);

	useEffect(() => {
		fetchDataContact();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchDataContact = async () => {
		setLoadingContact(true);
		const query = { id_customer, showAll: true };
		return PicModule.read(query)
			.then((response) => {
				const new_response = response?.foundData?.map((item) => {
					return {
						id_customer: item.id_customer,
						name: item.name,
						position: item.position,
						phone: item.phone,
						email: item.email,
					};
				});
				onChange(new_response);
			})
			.catch(() => {})
			.finally(() => {
				setLoadingContact(false);
			});
	};

	return loadingContact ? (
		<div className={classNames('py-1', 'd-flex', 'justify-content-center')}>
			<Spinner color='info' size='4rem' />
		</div>
	) : (
		<div style={{ overflow: 'auto', maxHeight: '320px' }}>
			<table className={classNames('table', 'table-modern')}>
				<thead>
					<tr>
						<th scope='col'>No</th>
						<th scope='col'>Name</th>
						<th scope='col'>Position</th>
						<th scope='col'>Phone</th>
						<th scope='col'>Email</th>
					</tr>
				</thead>
				<tbody>
					{data.map((item, index) => (
						<tr key={'pic-row'.concat(index)}>
							<th scope='col'>{index + 1}</th>
							<td>{item?.name}</td>
							<td>{item?.position}</td>
							<td>{item?.phone}</td>
							<td>{item?.email}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

PicComponent.propTypes = {
	id_customer: PropTypes.string,
	data: PropTypes.instanceOf(Array),
	onChange: PropTypes.func,
};
PicComponent.defaultProps = {
	id_customer: '',
	data: [],
	onChange: () => {},
};

export default PicComponent;
