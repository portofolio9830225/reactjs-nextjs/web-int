import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import TransaksiModule from '../../modules/v2/TransaksiModule';
import Spinner from '../../components/bootstrap/Spinner';

const TransactionComponent = ({ id_customer, data, onChange }) => {
	const [loadingContact, setLoadingContact] = useState(false);

	useEffect(() => {
		fetchDataContact();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchDataContact = async () => {
		setLoadingContact(true);
		const query = { id_customer, showAll: true };
		return TransaksiModule.read(query)
			.then((response) => {
				const new_response = response?.foundData?.map((item) => {
					return {
						id_customer: item.id_customer,
						date: item.date,
						product: item.product,
						packaging: item.packaging,
						fob_price: item.fob_price,
						qty: item.qty,
						sales_person: item.sales_person,
					};
				});
				onChange(new_response);
			})
			.catch(() => {})
			.finally(() => {
				setLoadingContact(false);
			});
	};

	return loadingContact ? (
		<div className={classNames('py-1', 'd-flex', 'justify-content-center')}>
			<Spinner color='info' size='4rem' />
		</div>
	) : (
		<div style={{ overflow: 'auto', maxHeight: '320px' }}>
			<table className={classNames('table', 'table-modern')}>
				<thead>
					<tr>
						<th scope='col'>No</th>
						<th scope='col'>Date</th>
						<th scope='col'>Product</th>
						<th scope='col'>Packaging Size (KG)</th>
						<th scope='col'>FOB Price/KG</th>
						<th scope='col'>QTY</th>
						<th scope='col'>Sales Person</th>
					</tr>
				</thead>
				<tbody>
					{data.map((item, index) => (
						<tr key={'pic-row'.concat(index)}>
							<th scope='col'>{index + 1}</th>
							<td>{item?.date}</td>
							<td>{item?.product}</td>
							<td>{item?.packaging}</td>
							<td>{item?.fob_price}</td>
							<td>{item?.qty}</td>
							<td>{item?.sales_person}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

TransactionComponent.propTypes = {
	id_customer: PropTypes.string,
	data: PropTypes.instanceOf(Array),
	onChange: PropTypes.func,
};
TransactionComponent.defaultProps = {
	id_customer: '',
	data: [],
	onChange: () => {},
};

export default TransactionComponent;
