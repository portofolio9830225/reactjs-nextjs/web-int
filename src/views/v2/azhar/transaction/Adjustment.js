import React, { useEffect, useState } from 'react';

import { useFormik } from 'formik';
import moment from 'moment';

import Page from '../../../../layout/Page/Page';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import Checks from '../../../../components/bootstrap/forms/Checks';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import Button from '../../../../components/bootstrap/Button';
import Input from '../../../../components/bootstrap/forms/Input';
import CustomSelect from '../../../../components/CustomSelect';
import showNotification from '../../../../components/extras/showNotification';

import useDarkMode from '../../../../hooks/useDarkMode';
import transaksiModule from '../../../../modules/v2/TransaksiModule';
import customerModule from '../../../../modules/v2/CustomerModule';
import productModule from '../../../../modules/v2/azhar/ProductModule';

import { isEmptyObject } from '../../../../helpers/helpers';

const FilterTableCustom = (dt) => {
	const { fetch } = dt;
	const { darkModeStatus } = useDarkMode();

	const [newtype, setType] = useState({
		value: 'customer_code',
		text: 'Customer Code',
		label: 'Customer Code',
	});

	const formik = useFormik({
		initialValues: {
			category: '',
			keyword: '',
			start: '',
			end: '',
		},
		validate: (values) => {
			const errors = {};

			if (moment(moment(values.start)).isAfter(moment(values.end))) {
				errors.start = 'the date cannot be later than the end date';
				errors.end = 'the date cannot be less than the start date';
			}

			if (isEmptyObject(values.keyword)) {
				errors.keyword = 'required';
			}

			return errors;
		},
		onSubmit: (values) => {
			values.category = newtype.value;

			let query = `&category=${values.category}&keyword=${values.keyword}`;

			if (!isEmptyObject(values.start) && !isEmptyObject(values.start)) {
				query += `&start=${values.start}&end=${values.end}`;
			}

			fetch(1, 10, query);
		},
	});

	const listType = [
		{
			value: 'customer_code',
			text: 'Customer Code',
			label: 'Customer Code',
		},
		{
			value: 'product',
			text: 'Product',
			label: 'Product',
		},
		{
			value: 'country',
			text: 'Country',
			label: 'Country',
		},
		{
			value: 'region',
			text: 'Region',
			label: 'Region',
		},
	];

	const customHandleReset = (e) => {
		formik.handleReset(e);
		formik.values.category = '';

		setType({
			value: 'customer_code',
			text: 'Customer Code',
			label: 'Customer Code',
		});

		fetch(1, 10, '');
	};

	return (
		<form noValidate onSubmit={formik.handleSubmit} className='row'>
			<div className='col-sm-2 m-1'>
				<FormGroup id='category' label='Category'>
					<CustomSelect
						placeholder='Category'
						onChange={(e) => setType(e)}
						value={newtype}
						options={listType}
						isSearchable
						darkTheme={darkModeStatus}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='keyword' label='Keyword'>
					<Input
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.keyword}
						isTouched={formik.touched.keyword}
						invalidFeedback={formik.errors.keyword}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='start' label='Start'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.start}
						isTouched={formik.touched.start}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.start}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-2 m-1'>
				<FormGroup id='end' label='End'>
					<Input
						type='date'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.end}
						isTouched={formik.touched.end}
						isValid={formik.isValid}
						invalidFeedback={formik.errors.end}
					/>
				</FormGroup>
			</div>
			<div className='col-sm-3 m-1 d-flex align-items-end'>
				<Button icon='FilterAlt' color='success' type='submit' className='m-1'>
					Filter
				</Button>
				<Button
					icon='Clear'
					color='danger'
					type='reset'
					className='m-1'
					onClick={customHandleReset}>
					Clear
				</Button>
			</div>
		</form>
	);
};

const CustomDataTable = (dt) => {
	const {
		data,
		loading,
		customer,
		product,
		selectCS,
		setSelectCS,
		checkbox,
		setCheckbox,
		selectProduct,
		setSelectProduct,
		lists,
		setLists,
		preview,
		setPreview,
		fetch,
	} = dt;
	const { darkModeStatus } = useDarkMode();

	const handleSelect = (e, index) => {
		const new_data = [];
		const new_data_cek = [];
		const new_data_list = [...lists];

		selectCS.forEach((itm, i) => {
			if (i == index) {
				new_data_cek.push(true);
				new_data.push(e);

				new_data_list[i].customer_name = e.value;
			} else {
				new_data_cek.push(checkbox[i]);
				new_data.push(itm);
			}
		});

		setLists(new_data_list);
		setCheckbox(new_data_cek);
		setSelectCS(new_data);
		setPreview(true);
	};

	const handleProduct = (e, index) => {
		const new_data = [];
		const new_data_cek = [];
		const new_data_list = [...lists];

		selectProduct.forEach((itm, i) => {
			if (i == index) {
				new_data_cek.push(true);
				new_data.push(e);

				new_data_list[i].product = e.value;
			} else {
				new_data_cek.push(checkbox[i]);
				new_data.push(itm);
			}
		});

		setLists(new_data_list);
		setCheckbox(new_data_cek);
		setSelectProduct(new_data);
		setPreview(true);
	};

	const handleSubmit = () => {
		transaksiModule.createAdjust(lists).then((res) => {
			if (res.data == 'success') {
				showNotification('Success!', 'Success Update', 'success');
				fetch(1, 10, '');
			} else {
				showNotification('Warning!', 'Failed Update', 'danger');
			}
		});
	};

	return (
		<div className='mt-3'>
			{/* <div className='table-responsive' style={{ maxHeight: '500px' }}> */}
			<div style={{ overflowX: 'auto' }}>
				<table
					className='table table-modern'
					style={{ width: '2500px', marginBottom: '200px' }}>
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>Year</th>
							<th style={{ minWidth: '120px' }}>Periode</th>
							<th style={{ minWidth: '120px' }}>Customer</th>
							<th style={{ minWidth: '180px' }}>Customer Name</th>
							<th style={{ minWidth: '120px' }}>Country</th>
							<th style={{ minWidth: '120px' }}>Region</th>
							<th style={{ minWidth: '170px' }}>Product Category 1</th>
							<th style={{ minWidth: '170px' }}>Product Category 2</th>
							<th style={{ minWidth: '180px' }}>Product</th>
							<th style={{ minWidth: '150px' }}>Product Name</th>
							<th style={{ minWidth: '120px' }}>Quantity</th>
							<th style={{ minWidth: '120px' }}>Sales Value</th>
						</tr>
					</thead>
					<tbody>
						{loading ? (
							<tr>
								<th colSpan='13' align='center'>
									Loading...
								</th>
							</tr>
						) : (
							data.length > 0 &&
							data.map((e, i) => (
								<tr>
									<td>
										<Checks
											type='checkbox'
											id='is_employee'
											checked={checkbox[i]}
										/>
									</td>
									<td>{e.year}</td>
									<td>{e.periode}</td>
									<td>{e.customer_code}</td>
									<td>
										{e.customer_code == null || e.customer_code == '' ? (
											<CustomSelect
												placeholder='Customer'
												onChange={(el) => handleSelect(el, i)}
												value={selectCS[i]}
												options={customer}
												isSearchable
												darkTheme={darkModeStatus}
											/>
										) : (
											e.customer_name
										)}
									</td>
									<td>{e.country}</td>
									<td>{e.region}</td>
									<td>{e.product_category_1}</td>
									<td>{e.product_category_2}</td>
									<td>
										{e.product_code == null || e.product_code == '' ? (
											<CustomSelect
												placeholder='Product'
												onChange={(el) => handleProduct(el, i)}
												value={selectProduct[i]}
												options={product}
												isSearchable
												darkTheme={darkModeStatus}
											/>
										) : (
											e.product
										)}
									</td>
									<td>{e.product_name}</td>
									<td>{e.quantity}</td>
									<td>{e.product_category_1}</td>
								</tr>
							))
						)}
					</tbody>
				</table>
			</div>
			{preview && (
				<div className='col-md-12'>
					<Button
						icon='Save'
						isOutline
						type='button'
						color='success'
						className='float-end'
						onClick={() => handleSubmit()}>
						Save
					</Button>
				</div>
			)}
		</div>
	);
};

const Adjustment = () => {
	const [loading, setLoading] = useState(false);
	const [data, setData] = useState([]);

	const [customer, setCustomer] = useState([]);
	const [product, setProduct] = useState([]);

	const [selectCS, setSelectCS] = useState([]);
	const [checkbox, setCheckbox] = useState([]);
	const [selectProduct, setSelectProduct] = useState([]);

	const [lists, setLists] = useState([]);
	const [preview, setPreview] = useState(false);

	const fetchData = async (page, perPage, query) => {
		setLoading(true);
		setPreview(false);

		return transaksiModule.readAdjust(page, perPage, query).then((results) => {
			const list2 = [];
			const list3 = [];
			const checkboxs = [];

			results.data.forEach((el) => {
				list2.push(null);
				list3.push({
					id: el._id,
					customer_name: '',
					product: '',
				});
				checkboxs.push(false);
			});

			setCheckbox(checkboxs);
			setSelectCS(list2);
			setSelectProduct(list2);
			setLists(list3);

			setData(results.data);
			setLoading(false);
		});
	};

	const fetchCustomer = async () => {
		return customerModule.readAll().then((results) => {
			const list = [];

			results.forEach((el) => {
				list.push({
					value: el.customer_name,
					text: el.customer_name,
					label: el.customer_name,
				});
			});

			setCustomer(list);
		});
	};

	const fetchProduct = async () => {
		return productModule.readAll().then((results) => {
			const list = [];
			results.forEach((el) => {
				list.push({
					value: el.product_code,
					text: el.product_code,
					label: `${el.product_code} - ${el.product_name}`,
				});
			});

			setProduct(list);
		});
	};

	useEffect(() => {
		fetchCustomer();
		fetchProduct();
	}, []);

	useEffect(() => {
		fetchData(1, 10, '');
	}, []);

	return (
		<PageWrapper title='Adjustment Transaction'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>Adjustment Page</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FilterTableCustom fetch={fetchData} />
							<CustomDataTable
								data={data}
								loading={loading}
								customer={customer}
								product={product}
								selectCS={selectCS}
								setSelectCS={setSelectCS}
								checkbox={checkbox}
								setCheckbox={setCheckbox}
								selectProduct={selectProduct}
								setSelectProduct={setSelectProduct}
								lists={lists}
								setLists={setLists}
								preview={preview}
								setPreview={setPreview}
								fetch={fetchData}
							/>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default Adjustment;
