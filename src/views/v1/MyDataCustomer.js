import React, { useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import moment from 'moment';
import FileDownload from 'js-file-download';
import Swal from 'sweetalert2';
import Accordion, { AccordionItem } from '../../components/bootstrap/Accordion';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../components/bootstrap/Card';
import CustomerModule from '../../modules/v1/CustomerModule';
import DarkDataTable from '../../components/DarkDataTable';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../../components/bootstrap/Modal';
import Page from '../../layout/Page/Page';
import PageWrapper from '../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../pages/common/Headers/PageLayoutHeader';
import Button from '../../components/bootstrap/Button';
import useDarkMode from '../../hooks/useDarkMode';
import FormGroup from '../../components/bootstrap/forms/FormGroup';
import Input from '../../components/bootstrap/forms/Input';
import Textarea from '../../components/bootstrap/forms/Textarea';
import { generateOptionsYear, getRequester } from '../../helpers/helpers';
import Worksheet, {
	readFileExcel,
	readFileExcelFormat,
	writeFileExcel,
} from '../../components/custom/Worksheet';
import Spinner from '../../components/bootstrap/Spinner';
import showNotification from '../../components/extras/showNotification';
import Select from '../../components/custom/Select';

const handleCreate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			CustomerModule.create(values)
				.then(() => {
					Swal.fire('Information!', 'Data has been saved successfully', 'success');
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Swal.fire('Warning!', err, 'error');
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const handleBulkCreate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			CustomerModule.createBulk(values)
				.then(() => {
					Swal.fire('Information!', 'Data has been saved successfully', 'success');
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Swal.fire('Warning!', err, 'error');
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			CustomerModule.update(values)
				.then(() => {
					Swal.fire('Information!', 'Data has been updated successfully', 'success');
					resolve({ error: false, message: 'successfully' });
				})
				.catch((err) => {
					Swal.fire('Warning!', err, 'error');
					reject(new Error(err));
				});
		} catch (e) {
			reject(new Error(e.message));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	// eslint-disable-next-line no-async-promise-executor
	const newResponse = new Promise(async (resolve, reject) => {
		try {
			const { value: text } = await Swal.fire({
				input: 'textarea',
				inputLabel: 'Reason',
				inputPlaceholder: 'Input your reaseon here...',
				inputAttributes: {
					'aria-label': 'Input your reason here',
				},
				showCancelButton: true,
				// eslint-disable-next-line consistent-return
				inputValidator: (value) => {
					if (!value) {
						return 'You need to write something!';
					}
				},
			});

			if (text) {
				const { username } = getRequester();
				const newValues = { _id: values._id, requester: username };
				newValues.remarks = text;

				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!',
				}).then((result) => {
					if (result.isConfirmed) {
						CustomerModule.destroy(newValues)
							.then(() => {
								Swal.fire(
									'Information!',
									'Data has been deleted successfully',
									'success',
								);
								resolve({ error: false, message: 'successfully' });
							})
							.catch((err) => {
								Swal.fire('Warning!', err, 'error');
								reject(new Error(err));
							});
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
						reject(new Error('Cancelled'));
					}
				});
			}
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const CustomModalAddPerson = ({ isOpen, setOpen, onChange }) => {
	const { darkModeStatus } = useDarkMode();

	const formik = useFormik({
		initialValues: { name: '', position: '', email: '' },
		validate: (values) => {
			const errors = {};

			if (!values.name) {
				errors.name = 'Required';
			}

			if (!values.position) {
				errors.position = 'Required';
			}

			if (!values.email) {
				errors.email = 'Required';
			} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
				errors.email = 'Invalid email address';
			}

			return errors;
		},
		onSubmit: (values) => {
			onChange(values);
			setOpen(false);
		},
	});

	return (
		<Modal isOpen={isOpen} setIsOpen={setOpen} titleId='modal-crud'>
			<ModalHeader setIsOpen={setOpen} className='p-4'>
				<ModalTitle id='modal-crud'>Add Contact Person</ModalTitle>
			</ModalHeader>
			<ModalBody>
				<div tag='form' noValidate onSubmit={formik.handleSubmit}>
					<div className='row p-1'>
						<div className='col-md-12 py-2'>
							<FormGroup id='name' label='Name'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.name}
									isValid={formik.isValid}
									isTouched={formik.touched.name}
									invalidFeedback={formik.errors.name}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-12 py-2'>
							<FormGroup id='position' label='Position'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.position}
									isValid={formik.isValid}
									isTouched={formik.touched.position}
									invalidFeedback={formik.errors.position}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-12 py-2'>
							<FormGroup id='email' label='Email'>
								<Input
									type='email'
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.email}
									isValid={formik.isValid}
									isTouched={formik.touched.email}
									invalidFeedback={formik.errors.email}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
					</div>
					<div className='row'>
						<div className='col-md-12'>
							<Button
								icon='Save'
								color='success'
								type='button'
								size='sm'
								className='float-end'
								onClick={formik.handleSubmit}
								isLight={darkModeStatus}>
								Save
							</Button>
						</div>
					</div>
				</div>
			</ModalBody>
		</Modal>
	);
};

CustomModalAddPerson.propTypes = {
	isOpen: PropTypes.bool,
	setOpen: PropTypes.func,
	onChange: PropTypes.func,
};
CustomModalAddPerson.defaultProps = {
	isOpen: false,
	setOpen: () => {},
	onChange: () => {},
};

const CustomModalAddDetail = ({ isOpen, setOpen, onChange }) => {
	const { darkModeStatus } = useDarkMode();

	const formik = useFormik({
		initialValues: {
			product_grade: '',
			packing_size: '',
			fob_price: '',
			qty_year_1: '',
			qty_year_2: '',
			full_potential: '',
			potential_per_month: '',
			potential_per_year: '',
			share_year_1: '',
			share_year_2: '',
			remarks: '',
		},
		validate: () => {
			const errors = {};

			return errors;
		},
		onSubmit: (values) => {
			onChange(values);
			setOpen(false);
		},
	});

	return (
		<Modal isOpen={isOpen} setIsOpen={setOpen} titleId='modal-crud' size='lg'>
			<ModalHeader setIsOpen={setOpen} className='p-4'>
				<ModalTitle id='modal-crud'>Add Detail Product</ModalTitle>
			</ModalHeader>
			<ModalBody>
				<div tag='form' noValidate onSubmit={formik.handleSubmit}>
					<div className='row p-1'>
						<div className='col-md-6 py-2'>
							<FormGroup id='product_grade' label='Product Grade'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.product_grade}
									isValid={formik.isValid}
									isTouched={formik.touched.product_grade}
									invalidFeedback={formik.errors.product_grade}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='packing_size' label='Packing Size'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.packing_size}
									isValid={formik.isValid}
									isTouched={formik.touched.packing_size}
									invalidFeedback={formik.errors.packing_size}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='qty_year_1' label='QTY Year 1'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.qty_year_1}
									isValid={formik.isValid}
									isTouched={formik.touched.qty_year_1}
									invalidFeedback={formik.errors.qty_year_1}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='qty_year_2' label='QTY Year 2'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.qty_year_2}
									isValid={formik.isValid}
									isTouched={formik.touched.qty_year_2}
									invalidFeedback={formik.errors.qty_year_2}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='fob_price' label='F.O.B Price'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.fob_price}
									isValid={formik.isValid}
									isTouched={formik.touched.fob_price}
									invalidFeedback={formik.errors.fob_price}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='full_potential' label='Full Potential'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.full_potential}
									isValid={formik.isValid}
									isTouched={formik.touched.full_potential}
									invalidFeedback={formik.errors.full_potential}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='potential_per_month' label='Potential Per Month'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.potential_per_month}
									isValid={formik.isValid}
									isTouched={formik.touched.potential_per_month}
									invalidFeedback={formik.errors.potential_per_month}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-6 py-2'>
							<FormGroup id='potential_per_year' label='Potential Per Year'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.potential_per_year}
									isValid={formik.isValid}
									isTouched={formik.touched.potential_per_year}
									invalidFeedback={formik.errors.potential_per_year}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-12 py-2'>
							<FormGroup id='remarks' label='Remarks'>
								<Input
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.remarks}
									isValid={formik.isValid}
									isTouched={formik.touched.remarks}
									invalidFeedback={formik.errors.remarks}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
					</div>
					<div className='row'>
						<div className='col-md-12'>
							<Button
								icon='Save'
								color='success'
								type='button'
								size='sm'
								className='float-end'
								onClick={formik.handleSubmit}
								isLight={darkModeStatus}>
								Save
							</Button>
						</div>
					</div>
				</div>
			</ModalBody>
		</Modal>
	);
};

CustomModalAddDetail.propTypes = {
	isOpen: PropTypes.bool,
	setOpen: PropTypes.func,
	onChange: PropTypes.func,
};
CustomModalAddDetail.defaultProps = {
	isOpen: false,
	setOpen: () => {},
	onChange: () => {},
};

const FormCustom = ({ initialValues, isReadOnly, isUpdate, options, handleCustomSubmit }) => {
	const { darkModeStatus } = useDarkMode();
	const { username, department } = getRequester();

	const [isShowAddPerson, setShowAddPerson] = useState(false);
	const [isShowAddDetail, setShowAddDetail] = useState(false);

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate: () => {
			const errors = {};

			return errors;
		},
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				// values.contact_detail = values.contact_detail.split('\n');
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						const new_values = {};
						if (isUpdate) {
							new_values._id = values._id;
						}
						new_values.requester = username;
						new_values.department = department;
						new_values.contact_person = values.contact_person;
						new_values.contact_detail = values.contact_detail.split('\n');
						new_values.details = values.details;
						new_values.customer_name = values.customer_name;
						new_values.country = values.country;
						new_values.top = values.top;
						new_values.comercial_lost = values.comercial_lost;
						new_values.qty_year_1 = values.qty_year_1;
						new_values.qty_year_2 = values.qty_year_2;
						new_values.sales_person = values.sales_person;

						handleCustomSubmit(new_values, options, resetForm);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const handleRemovePerson = (index) => {
		const new_data = [...formik.values.contact_person];
		new_data.splice(index, 1);
		formik.setFieldValue('contact_person', new_data);
	};

	const handleRemoveDetail = (index) => {
		const new_data = [...formik.values.details];
		new_data.splice(index, 1);
		formik.setFieldValue('details', new_data);
	};

	return (
		<div className='p-1' tag='form' noValidate onSubmit={formik.handleSubmit}>
			<div className='row py-1'>
				<div className='col-md-12'>
					<Card shadow='sm'>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='Assignment' iconColor='info'>
								<CardTitle>General Information</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<div className='row p-1'>
								<div className='col-md-12 py-2'>
									<FormGroup
										id='customer_name'
										label='Customer Name'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.customer_name}
											isValid={formik.isValid}
											isTouched={formik.touched.customer_name}
											invalidFeedback={formik.errors.customer_name}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>
								<div className='col-md-12 py-2'>
									<FormGroup
										id='top'
										label='T.O.P'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.top}
											isValid={formik.isValid}
											isTouched={formik.touched.top}
											invalidFeedback={formik.errors.top}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>
								<div className='col-md-12 py-2'>
									<FormGroup
										id='comercial_lost'
										label='Commercial Lost'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.comercial_lost}
											isValid={formik.isValid}
											isTouched={formik.touched.comercial_lost}
											invalidFeedback={formik.errors.comercial_lost}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>
								<div className='col-md-12 py-2'>
									<FormGroup
										id='sales_person'
										label='Sales Person'
										isColForLabel
										labelClassName='col-sm-2 text-capitalize'
										childWrapperClassName='col-sm-10'>
										<Input
											onChange={formik.handleChange}
											onBlur={formik.handleBlur}
											value={formik.values.sales_person}
											isValid={formik.isValid}
											isTouched={formik.touched.sales_person}
											invalidFeedback={formik.errors.sales_person}
											autoComplete='off'
											disabled={isReadOnly}
										/>
									</FormGroup>
								</div>
							</div>
						</CardBody>
					</Card>
				</div>

				<div className='col-md-8'>
					<Card shadow='sm' stretch>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='Contacts' iconColor='info'>
								<CardTitle>Contact Person</CardTitle>
							</CardLabel>
							{!isReadOnly && (
								<CardActions>
									<Button
										icon='Add'
										color='success'
										type='button'
										size='sm'
										onClick={() => setShowAddPerson(true)}
										isLight={darkModeStatus}>
										Add
									</Button>
								</CardActions>
							)}
						</CardHeader>
						<CardBody>
							<table className='table table-modern'>
								<thead>
									<tr>
										<th scope='col'>No</th>
										<th scope='col'>Name</th>
										<th scope='col'>Position</th>
										<th scope='col'>Email</th>
										<th
											scope='col'
											style={{
												whiteSpace: 'nowrap',
												display: isReadOnly ? 'none' : '',
											}}>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									{formik.values?.contact_person?.map((item, index) => (
										<tr key={'person-row'.concat(index)}>
											<th key={'person-row'.concat(index, '-', 1)}>
												{index + 1}
											</th>
											<td key={'person-row'.concat(index, '-', 2)}>
												{item?.name}
											</td>
											<td key={'person-row'.concat(index, '-', 3)}>
												{item?.position}
											</td>
											<td key={'person-row'.concat(index, '-', 4)}>
												{item?.email}
											</td>
											<td
												key={'person-row'.concat(index, '-', 5)}
												style={{ display: isReadOnly ? 'none' : '' }}>
												<Button
													icon='Delete'
													color='danger'
													type='button'
													size='sm'
													onClick={() => handleRemovePerson(index)}
													isLight={darkModeStatus}
												/>
											</td>
										</tr>
									))}
								</tbody>
							</table>
						</CardBody>
					</Card>
				</div>

				<div className='col-md-4'>
					<Card shadow='sm' stretch>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='ManageSearch' iconColor='info'>
								<CardTitle>Contact Detail</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<FormGroup id='contact_detail'>
								<Textarea
									value={formik.values.contact_detail}
									onChange={formik.handleChange}
									rows={6}
									disabled={isReadOnly}
								/>
							</FormGroup>
						</CardBody>
					</Card>
				</div>

				<div className='col-md-12'>
					<Card shadow='sm'>
						<CardHeader borderSize={1} size='sm'>
							<CardLabel icon='LibraryBooks' iconColor='info'>
								<CardTitle>Detail Product</CardTitle>
							</CardLabel>
							{!isReadOnly && (
								<CardActions>
									<Button
										icon='Add'
										color='success'
										type='button'
										size='sm'
										onClick={() => setShowAddDetail(true)}
										isLight={darkModeStatus}>
										Add
									</Button>
								</CardActions>
							)}
						</CardHeader>
						<CardBody>
							<table className='table table-modern'>
								<thead>
									<tr>
										<th scope='col'>No</th>
										<th scope='col'>Product Grade</th>
										<th scope='col'>Packing Size (Kg)</th>
										<th scope='col'>FOB Price</th>
										<th scope='col'>QTY Year {formik.values.qty_year_1}</th>
										<th scope='col'>QTY Year {formik.values.qty_year_2}</th>
										<th scope='col'>Full Potential</th>
										<th scope='col'>Potential Per Month</th>
										<th scope='col'>Potential Per Year</th>
										<th scope='col'>Remark</th>
										<th
											scope='col'
											style={{
												whiteSpace: 'nowrap',
												display: isReadOnly ? 'none' : '',
											}}>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									{formik.values?.details?.map((item, index) => (
										<tr key={'detail-row'.concat(index)}>
											<th key={'detail-row'.concat(index, '-', 1)}>
												{index + 1}
											</th>
											<td key={'detail-row'.concat(index, '-', 2)}>
												{item?.product_grade}
											</td>
											<td key={'detail-row'.concat(index, '-', 3)}>
												{item?.packing_size}
											</td>
											<td key={'detail-row'.concat(index, '-', 4)}>
												{item?.fob_price}
											</td>
											<td key={'detail-row'.concat(index, '-', 5)}>
												{item?.qty_year_1}
											</td>
											<td key={'detail-row'.concat(index, '-', 6)}>
												{item?.qty_year_2}
											</td>
											<td key={'detail-row'.concat(index, '-', 7)}>
												{item?.qty_year_2}
											</td>
											<td key={'detail-row'.concat(index, '-', 8)}>
												{item?.full_potential}
											</td>
											<td key={'detail-row'.concat(index, '-', 9)}>
												{item?.potential_per_month}
											</td>
											<td key={'detail-row'.concat(index, '-', 10)}>
												{item?.potential_per_year}
											</td>
											<td
												key={'detail-row'.concat(index, '-', 11)}
												style={{ display: isReadOnly ? 'none' : '' }}>
												<Button
													icon='Delete'
													color='danger'
													type='button'
													size='sm'
													onClick={() => handleRemoveDetail(index)}
													isLight={darkModeStatus}
												/>
											</td>
										</tr>
									))}
								</tbody>
							</table>
						</CardBody>
					</Card>
				</div>
			</div>

			{!isReadOnly && (
				<div className='row py-1'>
					<div className='col'>
						<Button
							icon='Save'
							color='success'
							type='button'
							onClick={formik.handleSubmit}
							isLight={darkModeStatus}
							className='float-end m-1'>
							Submit
						</Button>

						{!isUpdate && (
							<Button
								icon='Clear'
								color='danger'
								type='button'
								isLight={darkModeStatus}
								className='float-end m-1'>
								Clear
							</Button>
						)}
					</div>
				</div>
			)}

			<CustomModalAddPerson
				isOpen={isShowAddPerson}
				setOpen={setShowAddPerson}
				onChange={(e) => {
					const new_data = [...formik.values.contact_person];
					new_data.push(e);
					formik.setFieldValue('contact_person', new_data);
				}}
			/>

			<CustomModalAddDetail
				isOpen={isShowAddDetail}
				setOpen={setShowAddDetail}
				onChange={(e) => {
					const new_data = [...formik.values.details];
					new_data.push(e);
					formik.setFieldValue('details', new_data);
				}}
			/>
		</div>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	isReadOnly: PropTypes.bool,
	isUpdate: PropTypes.bool,
	options: PropTypes.instanceOf(Object),
	handleCustomSubmit: PropTypes.func,
};
FormCustom.defaultProps = {
	initialValues: null,
	isReadOnly: false,
	isUpdate: false,
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	handleCustomSubmit: () => {},
};

const FormCustomImport = ({ initialValues, options, handleCustomSubmit }) => {
	const { darkModeStatus } = useDarkMode();
	const { username, department } = getRequester();

	const [loadingImport, setLoadingImport] = useState(false);
	const [loadingGenerate, setLoadingGenerate] = useState(false);

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate: (values) => {
			const errors = {};

			if (values?.form_file?.split('.')?.pop() !== 'xlsx') {
				errors.form_file = 'File must be .xlsx';
			}

			return errors;
		},
		onReset: () => {},
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						onCustomSubmitBulk(values, { setSubmitting, resetForm, setErrors });
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const onCustomSubmitBulk = (values, { setSubmitting, resetForm, setErrors }) => {
		try {
			const file_name = document.getElementById('form_file');
			if (file_name?.files?.length) {
				const fileObject = file_name.files[0];

				setLoadingImport(true);

				readFileExcelFormat(fileObject)
					.then(({ customer, customer_opt }) => {
						const list_customer = getDataCustomerBulk(customer, customer_opt);

						if (list_customer.length === 0) {
							showNotification('Information', 'Data Customer is null', 'danger');
							setLoadingImport(false);
							return;
						}

						const result_check = check_data_parameter(list_customer);

						if (result_check.error) {
							showNotification('Information', result_check.message, 'danger');
							setLoadingImport(false);
							return;
						}

						const new_values = {};
						new_values.requester = username;
						new_values.department = department;
						new_values.data = list_customer;

						handleCustomSubmit(new_values, options, resetForm, setLoadingImport);
					})
					.catch(() => {
						setLoadingImport(false);
					})
					.finally(() => {});
			}
		} catch (error) {
			setErrors({ submit: error.message });
		} finally {
			setSubmitting(false);
		}
	};

	const handleCustomChangeFile = (e) => {
		formik.handleChange(e);

		if (e?.target?.files?.length) {
			const fileObject = e.target.files[0];

			readFileExcel(fileObject)
				.then((response) => {
					formik.setFieldValue('data', response);
				})
				.catch(() => {
					formik.setFieldValue('data', []);
				});
		}
	};

	const handleCustomGenerate = async () => {
		setLoadingGenerate(true);

		writeFileExcel()
			.then((response) => {
				FileDownload(response.file, response.file_name);
			})
			.catch(() => {})
			.finally(() => {
				setLoadingGenerate(false);
			});
	};

	const getDataCustomerBulk = (_data_customer_bulk, _data_customer_bulk_opt) => {
		const data_customer = [];

		if (_data_customer_bulk.length === 0) {
			return [];
		}

		_data_customer_bulk.forEach((customer) => {
			const new_customer = getDataCustomer(customer);

			const _new_customer = {
				...new_customer,
				qty_year_1: Number(_data_customer_bulk_opt.qty_year_1),
				qty_year_2: Number(_data_customer_bulk_opt.qty_year_2),
			};

			data_customer.push(_new_customer);
		});

		return data_customer;
	};

	const getDataCustomer = (_data_customer) => {
		const new_data_customer = {};
		const contact_person = [];
		const contact_detail = [];
		const details = [];

		_data_customer.forEach((item) => {
			// init start
			// field customer name
			if (!new_data_customer.customer_name && item.customer_name) {
				new_data_customer.customer_name = item.customer_name;
			}
			// field country
			if (!new_data_customer.country && item.country) {
				new_data_customer.country = item.country;
			}
			// field contact person & position & email
			if (item.contact_person && !item.contact_person.includes('--')) {
				const new_contact_person = {};
				new_contact_person.name = item.contact_person;
				if (item.position) {
					new_contact_person.position = item.position;
				}
				if (item.email_phone && item.email_phone.includes('@')) {
					new_contact_person.email = item.email_phone;
				} else {
					contact_detail.push(item.email_phone);
				}
				contact_person.push(new_contact_person);
			}
			// field contact
			if (!item.contact_person && !item.position && item.email_phone) {
				contact_detail.push(item.email_phone);
			}
			// field top
			if (!new_data_customer.top && item.top) {
				new_data_customer.top = item.top;
			}
			// field top
			if (!new_data_customer.comercial_lost && item.comercial_lost) {
				new_data_customer.comercial_lost = item.comercial_lost;
			}
			// field sales person
			if (!new_data_customer.sales_person && item.sales_person) {
				new_data_customer.sales_person = item.sales_person;
			}

			// init details
			const new_details = {};
			// product grade
			if (item.product_grade) {
				new_details.product_grade = item.product_grade;
			}
			// packing_size (KG)
			if (item.packing_size) {
				new_details.packing_size = item.packing_size;
			}
			// fob_price ($)
			if (item.fob_price) {
				new_details.fob_price = item.fob_price;
			}
			// qty_year_1 (MT)
			if (item.qty_year_1) {
				new_details.qty_year_1 = item.qty_year_1;
			}
			// qty_year_2 (MT)
			if (item.qty_year_2) {
				new_details.qty_year_2 = item.qty_year_2;
			}
			// full_potential
			if (item.full_potential) {
				new_details.full_potential = item.full_potential;
			}
			// potential_per_month
			if (item.potential_per_month) {
				new_details.potential_per_month = item.potential_per_month;
			}
			// potential_per_year
			if (item.potential_per_year) {
				new_details.potential_per_year = item.potential_per_year;
			}
			// share_year_1
			if (item.share_year_1) {
				new_details.share_year_1 = item.share_year_1;
			}
			// share_year_2
			if (item.share_year_2) {
				new_details.share_year_2 = item.share_year_2;
			}
			// remarks
			if (item.remarks) {
				new_details.remarks = item.remarks;
			}

			if (Object.keys(new_details).length !== 0) {
				details.push(new_details);
			}
		});

		new_data_customer.contact_person = contact_person;
		new_data_customer.contact_detail = contact_detail;
		new_data_customer.details = details;

		return new_data_customer;
	};

	const check_data_parameter = (_list_customer) => {
		let status = { error: false, message: '' };

		for (let index = 0; index < _list_customer.length; index += 1) {
			if (!_list_customer.at(index).customer_name) {
				status = { error: true, message: 'customer name is required' };
				break;
			}
			if (!_list_customer.at(index).country) {
				status = { error: true, message: 'country is required' };
				break;
			}
		}

		return status;
	};

	return (
		<div className='p-1' tag='form' noValidate onSubmit={formik.handleSubmit}>
			<div className='row py-2'>
				<div className='col'>
					<FormGroup id='form_file' label='File'>
						<Input
							id='form_file'
							type='file'
							onChange={handleCustomChangeFile}
							onBlur={formik.handleBlur}
							value={formik.values.form_file}
							isValid={formik.isValid}
							isTouched={formik.touched.form_file}
							invalidFeedback={formik.errors.form_file}
						/>
					</FormGroup>
				</div>
			</div>

			{formik.values?.data?.length !== 0 && (
				<div className='row'>
					<div className='col-md-12'>
						<Worksheet data={formik.values?.data} />
					</div>
				</div>
			)}

			<div className='row py-2'>
				<div className='col'>
					{loadingGenerate ? (
						<Button color='info' className='float-start m-1' isLight={darkModeStatus}>
							<Spinner isSmall inButton />
							Loading...
						</Button>
					) : (
						<Button
							icon='CloudDownload'
							color='info'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomGenerate}
							className='float-start m-1'>
							Download
						</Button>
					)}

					{loadingImport ? (
						<Button color='success' className='float-end m-1' isLight={darkModeStatus}>
							<Spinner isSmall inButton />
							Loading...
						</Button>
					) : (
						<Button
							icon='Save'
							color='success'
							type='button'
							onClick={formik.handleSubmit}
							isLight={darkModeStatus}
							className='float-end m-1'>
							Import
						</Button>
					)}

					<Button
						icon='Clear'
						color='danger'
						type='button'
						onClick={formik.handleReset}
						isLight={darkModeStatus}
						className='float-end m-1'>
						Clear
					</Button>
				</div>
			</div>
		</div>
	);
};

FormCustomImport.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	options: PropTypes.instanceOf(Object),
	handleCustomSubmit: PropTypes.func,
};
FormCustomImport.defaultProps = {
	initialValues: {
		form_file: '',
		data: [],
	},
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	handleCustomSubmit: () => {},
};

const FormCustomModal = ({ initialValues, options, handleCustomUpdate }) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);
	const [isReadOnly, setReadOnly] = useState(false);

	const [title, setTitle] = useState('');

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Detail');
					setReadOnly(true);
					setOpen(true);
				}}
			/>

			{/* AKAN DIAKTIFKAN BILA DIPERLUKAN */}
			<Button
				icon='Edit'
				color='success'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Update');
					setReadOnly(false);
					setOpen(true);
				}}
			/>

			<Modal
				isOpen={isOpen}
				setIsOpen={setOpen}
				titleId='modal-crud'
				size='xl'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title}</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						options={options}
						isReadOnly={isReadOnly}
						isUpdate={isOpen}
						handleCustomSubmit={handleCustomUpdate}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	options: PropTypes.instanceOf(Object),
	handleCustomUpdate: PropTypes.func,
};
FormCustomModal.defaultProps = {
	initialValues: null,
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	handleCustomUpdate: () => {},
};

const ButtonCustom = ({ initialValues, options, handleCustomUpdate }) => {
	const initModal = {
		...initialValues,
		contact_detail: initialValues.contact_detail?.join('\n'),
	};

	return (
		<>
			<FormCustomModal
				initialValues={initModal}
				options={options}
				handleCustomUpdate={handleCustomUpdate}
			/>

			{/* AKAN DIAKTIFKAN BILA DIPERLUKAN */}
			{/* <Button
				icon='Delete'
				color='danger'
				type='button'
				className='mx-1'
				isLight={darkModeStatus}
				onClick={() => handleCustomDelete(initialValues)}
			/> */}
		</>
	);
};

ButtonCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	options: PropTypes.instanceOf(Object),
	handleCustomUpdate: PropTypes.func,
	// handleCustomDelete: PropTypes.func,
};
ButtonCustom.defaultProps = {
	initialValues: null,
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	handleCustomUpdate: () => {},
	// handleCustomDelete: () => {},
};

const TableCustom = ({
	data,
	totalRows,
	options,
	loading,
	isReset,
	fetchData,
	handleCustomUpdate,
	handleCustomDelete,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, options.perPage, options.showAll, options.filter);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = newPerPage === totalRows;
		return fetchData(page, newPerPage, all, options.filter);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'COUNTRY',
				cell: (row) => row.country,
				sortable: true,
				width: '180px',
			},
			{
				name: 'CUSTOMER',
				cell: (row) => row.customer_name,
				sortable: true,
				width: '360px',
			},
			{
				name: 'QTY YEAR',
				cell: (row) => row.qty_year_2,
				sortable: true,
				width: '130px',
			},
			{
				name: 'CREATED',
				cell: (row) => moment(row?.created_at).format('YYYY-MM-DD HH:mm'),
				sortable: true,
				width: '160px',
			},
			{
				name: 'ACTION',
				width: '180px',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const initialValues = row;

					return (
						<ButtonCustom
							initialValues={initialValues}
							options={options}
							handleCustomUpdate={handleCustomUpdate}
							handleCustomDelete={handleCustomDelete}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[options],
	);
	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			progressComponent={
				<div className='pt-5'>
					<Spinner color='info' size='8rem' />
				</div>
			}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			paginationResetDefaultPage={isReset}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	loading: PropTypes.bool,
	isReset: PropTypes.bool,
	options: PropTypes.instanceOf(Object),
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	totalRows: 0,
	loading: false,
	isReset: false,
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	fetchData: () => {},
	handleCustomUpdate: () => {},
	handleCustomDelete: () => {},
};

const FilterCustom = ({ options, fetchData }) => {
	const { darkModeStatus } = useDarkMode();

	const formik = useFormik({
		initialValues: { description: '', year: '' },
		onReset: () => {
			fetchData(1, options.perPage, options.showAll, {}, true);
		},
		onSubmit: (values) => {
			const filter = { description: values.description, year: values.year?.value };
			fetchData(1, options.perPage, options.showAll, filter);
		},
	});

	const listYear = generateOptionsYear();

	return (
		<div
			className='row p-1 d-flex align-items-end'
			tag='form'
			noValidate
			onSubmit={formik.handleSubmit}>
			<div className='col-md-3 py-1'>
				<FormGroup id='description' label='Description'>
					<Input
						placeholder='Country / Customer'
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.description}
						isValid={formik.isValid}
						isTouched={formik.touched.description}
						invalidFeedback={formik.errors.description}
						autoComplete='off'
					/>
				</FormGroup>
			</div>
			<div className='col-md-3 py-1'>
				<FormGroup id='year' label='Year'>
					<Select
						options={listYear}
						isSearchable={listYear.length > 7}
						onChange={(e) => formik.setFieldValue('year', e)}
						defaultValue={formik.values.year}
						value={formik.values.year}
						isClearable
					/>
				</FormGroup>
			</div>
			<div className='col-md-3 py-1'>
				<Button
					icon='Search'
					color='success'
					type='button'
					className='m-1'
					onClick={formik.handleSubmit}
					isLight={darkModeStatus}>
					Find
				</Button>

				<Button
					icon='Clear'
					color='danger'
					type='button'
					className='m-1'
					onClick={formik.handleReset}
					isLight={darkModeStatus}>
					Reset
				</Button>
			</div>
		</div>
	);
};

FilterCustom.propTypes = {
	options: PropTypes.instanceOf(Object),
	fetchData: PropTypes.func,
};
FilterCustom.defaultProps = {
	options: {
		perPage: 10,
		curPage: 1,
		showAll: false,
		filter: {},
	},
	fetchData: () => {},
};

const MyDataCustomer = () => {
	const { t } = useTranslation('crud');
	const { username, department } = getRequester();

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [showAll, setShowAll] = useState(false);
	const [loading, setLoading] = useState(false);
	const [filter, setFilter] = useState({});

	const [isReset, setIsReset] = useState(false);

	const initialValues = {
		contact_person: [],
		contact_detail: [],
		details: [],
		customer_name: '',
		country: '',
		top: '',
		comercial_lost: 0,
		qty_year_1: moment().subtract(1, 'years').format('YYYY'),
		qty_year_2: moment().format('YYYY'),
	};

	const initialValuesImport = {
		form_file: '',
		data: [],
	};

	const createSubmit = (values, options, resetForm) => {
		handleCreate(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll, options.filter);
				resetForm(initialValues);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const createBulkSubmit = (values, options, resetForm, setLoadingImport) => {
		handleBulkCreate(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll, options.filter);
				resetForm(initialValues);
			})
			.catch(() => {})
			.finally(() => {
				setLoadingImport(false);
			});
	};

	const updateSubmit = (values, options, resetForm) => {
		handleUpdate(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll, options.filter);
				resetForm(initialValues);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const deleteSubmit = (values, options) => {
		handleDelete(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll, options.filter);
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		fetchData(curPage, perPage, showAll, filter);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, newShowAll, newFilter = {}, reset = false) => {
		setShowAll(newShowAll);
		setFilter(newFilter);
		setLoading(true);
		setIsReset(reset);

		const query = {
			page: newPage,
			sizePerPage: newPerPage,
			showAll: newShowAll,
			requester: username,
			department,
			...newFilter,
		};

		return CustomerModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
				setIsReset(false);
			});
	};

	return (
		<PageWrapper title={t('Data Customer')}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<Accordion
							id='accordion-form'
							activeItemId='form'
							shadow='sm'
							color='info'
							stretch>
							<AccordionItem id='form-create' title={t('form')} icon='AddBox'>
								{/* AKAN DIAKTIFKAN BILA DIPERLUKAN */}
								{/* <Card shadow='sm' hasTab>
									<CardTabItem id='single_input' title='Single Input'>
										<FormCustom
											initialValues={initialValues}
											options={{ curPage, perPage, showAll, filter }}
											handleCustomSubmit={createSubmit}
										/>
									</CardTabItem>

									<CardTabItem id='import_excel' title='Import Excel'> */}
								<FormCustomImport
									initialValues={initialValuesImport}
									options={{ curPage, perPage, showAll, filter }}
									onSubmit={createSubmit}
									handleCustomSubmit={createBulkSubmit}
								/>
								{/* </CardTabItem>
								</Card> */}
							</AccordionItem>
						</Accordion>
					</CardBody>
				</Card>

				<Card stretch>
					<CardHeader borderSize={1} size='sm'>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<FilterCustom
							options={{ curPage, perPage, showAll, filter }}
							fetchData={fetchData}
						/>

						<TableCustom
							data={data}
							totalRows={totalRows}
							loading={loading}
							isReset={isReset}
							options={{ curPage, perPage, showAll, filter }}
							fetchData={fetchData}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

MyDataCustomer.propTypes = {};
MyDataCustomer.defaultProps = {};

export default MyDataCustomer;
