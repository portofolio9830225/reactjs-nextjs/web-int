# Set the base image  
FROM node:14-buster as builder

RUN apt-get update -y && apt-get install rsync -y  
# Specify where our app will live in the container
WORKDIR /app

# add the node_modules folder to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# Prepare the container for building React
COPY ./package*.json /app/
COPY ./yarn.lock /app/
# install dependencies
RUN yarn --silent

# Copy the React App to the container
COPY . /app

# build the app 
RUN yarn build


# STAGE 2 - build the final image using a nginx web server 
# distribution and copy the react build files
FROM nginx:alpine
COPY --from=builder /app/build /usr/share/nginx/html
COPY --from=builder /app/ /app
RUN rm -rf /app/node_modules

RUN apk add --no-cache bash
RUN apk add --no-cache nodejs yarn --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community

# needed this to make React Router work properly 
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Expose port 80 for HTTP Traffic 
EXPOSE 80

# start the nginx web server

CMD ["nginx", "-g", "daemon off;"]
